#!/bin/bash
# Copyright 2024 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================

echo "=============================================================================================================="
echo "Please run the script as: "
echo "bash scripts/run_distributed_train_msrun.sh [DATA_PATH] [RANK_SIZE]"
echo "For example: bash scripts/run_distributed_train_msrun.sh /path/dataset 8"
echo "It is better to use the absolute path."
echo "=============================================================================================================="

DATA_PATH=$1
RANK_SIZE=$2
export DATA_PATH=${DATA_PATH}
export RANK_SIZE=${RANK_SIZE}
export HCCL_CONNECT_TIMEOUT=600
ulimit -s 302400

EXEC_PATH=$(pwd)
CONFIG_PATH=${EXEC_PATH}/default_config.yaml

if [ ! -d "${DATA_PATH}" ]
then
    echo "ERROR: ${DATA_PATH} is not a valid path for dataset, please check."
    exit 0
fi

env > env.log
echo "start training"
msrun --bind_core=True --worker_num=8 --local_worker_num=8 --master_port 8118 \
      --log_dir=msrun_log --join=True --cluster_time_out=300 \
      train.py --run_distribute True --config_path=${CONFIG_PATH} --platform Ascend --dataset_path=${DATA_PATH} --rank_size ${RANK_SIZE} &> log.txt &

