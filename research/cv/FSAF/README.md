# Contents

* [Contents](#contents)
    * [FSAF Description](#retinanet-description)
        * [Model Architecture](#model-architecture)
        * [Dataset](#dataset)
    * [Environment Requirements](#environment-requirements)
    * [Quick Start](#quick-start)
        * [Prepare the model](#prepare-the-model)
        * [Run the scripts](#run-the-scripts)
    * [Script Description](#script-description)
        * [Script and Sample Code](#script-and-sample-code)
        * [Script Parameters](#script-parameters)
    * [Training](#training)
        * [Training Process](#training-process)
        * [Transfer Training](#transfer-training)
        * [Distribute training](#distribute-training)
    * [Evaluation](#evaluation)
        * [Evaluation Process](#evaluation-process)
            * [Evaluation on GPU](#evaluation-on-gpu)
        * [Evaluation result](#evaluation-result)
    * [Inference](#inference)
        * [Inference Process](#inference-process)
            * [Inference on GPU](#inference-on-gpu)
        * [Inference result](#inference-result)
   * [Model Description](#model-description)
        * [Performance](#performance)
   * [Description of Random Situation](#description-of-random-situation)
   * [ModelZoo Homepage](#modelzoo-homepage)

## [FSAF Description](#contents)

The FSAF is feature selective anchor-free module, a simple and effective
building block for single-shot object detectors. It can be plugged into
singleshot detectors with feature pyramid structure. The FSAF module addresses
two limitations brought up by the conventional anchor-based detection: 1)
heuristic-guided feature selection; 2) overlap-based anchor sampling.
The general concept of the FSAF module is online feature selection applied to
the training of multi-level anchor-free branches. Specifically, an anchor-free
branch is attached to each level of the feature pyramid, allowing box encoding
and decoding in the anchor-free manner at an arbitrary level.

Experimental results on the COCO detection track show that FSAF module performs
better than anchor-based counterparts while being faster. When working jointly
with anchor-based branches, the FSAF module robustly improves the baseline
RetinaNet by a large margin under various settings, while introducing nearly
free inference overhead. And the resulting best model can achieve a
state-of-the-art 44.6% mAP, outperforming all existing single-shot
detectors on COCO.

[Paper](https://arxiv.org/abs/1903.00621): Chenchen Zhu, Yihui He, Marios
Savvides. Computer Vision and Pattern Recognition (CVPR), 2019 (In press).

### [Model Architecture](#contents)

**Overview of the pipeline of FSAF:**
The FSAF algorithm is derived from the paper "Feature Selective Anchor-Free
Module for Single-Shot Object Detection" of Facebook AI Research in 2019. The
general concept of the FSAF module is online feature selection applied to the
training of multi-level anchor-free branches. Specifically, an anchor-free
branch is attached to each level of the feature pyramid, allowing box encoding
and decoding in the anchor-free manner at an arbitrary level.

FSAF result prediction pipeline:

1. ResNet.
2. FSAF head.
3. multiclass NMS (reduce number of proposed boxes and omit objects with low
 confidence).

RetinaNet result training pipeline:

1. ResNet.
2. FSAF head.
3. Assigner.
4. Classification + Localization losses.

### [Dataset](#contents)

Note that you can run the scripts based on the dataset mentioned in original
paper or widely used in relevant domain/network architecture. In the following
sections, we will introduce how to run the scripts using the related dataset
below.

Dataset used: [COCO-2017](https://cocodataset.org/#download)

* Dataset size: 25.4G
    * Train: 18.0G，118287 images
    * Val: 777.1M，5000 images
    * Test: 6.2G，40670 images
    * Annotations: 474.7M, represented in 3 JSON files for each subset.
* Data format: image and json files.
    * Note: Data will be processed in dataset.py

## [Environment Requirements](#contents)

* Install [MindSpore](https://www.mindspore.cn/install/en).
* Download the dataset COCO-2017.
* Install third-parties requirements:

```text
numpy~=1.21.2
opencv-python~=4.7.0.72
pycocotools~=2.0.6
matplotlib
seaborn
pandas
tqdm==4.64.1
```

* We use COCO-2017 as training dataset in this example by default, and you
 can also use your own datasets. Dataset structure:

```log
.
└── coco-2017
    ├── train
    │   ├── data
    │   │    ├── 000000000001.jpg
    │   │    ├── 000000000002.jpg
    │   │    └── ...
    │   └── labels.json
    ├── validation
    │   ├── data
    │   │    ├── 000000000001.jpg
    │   │    ├── 000000000002.jpg
    │   │    └── ...
    │   └── labels.json
    └── test
        ├── data
        │    ├── 000000000001.jpg
        │    ├── 000000000002.jpg
        │    └── ...
        └── labels.json
```

## [Quick Start](#contents)

### [Prepare the model](#contents)

1. Prepare yaml config file. Create file and copy content from
 `default_config.yaml` to created file.
2. Change data settings: experiment folder (`train_outputs`), image size
 settings (`img_width`, `img_height`, etc.), subsets folders (`train_dataset`,
 `val_dataset`), information about categories etc.
3. Change the backbone settings.
4. Change other training hyperparameters (learning rate, regularization,
 augmentations etc.).

### [Run the scripts](#contents)

After installing MindSpore via the official website, you can start training and
evaluation as follows:

* running on GPU

```shell
# distributed training on GPU
bash scripts/run_distribute_train_gpu.sh [CONFIG_PATH] [DEVICE_NUM] [TRAIN_DATA] [VAL_DATA] [TRAIN_OUT] [BRIEF] (OPTIONAL)[PRETRAINED_PATH]

# standalone training on GPU
bash scripts/run_standalone_train_gpu.sh [CONFIG_PATH] [TRAIN_DATA] [VAL_DATA] [TRAIN_OUT] [BRIEF] (OPTIONAL)[PRETRAINED_PATH]

# run eval on GPU
bash scripts/run_eval_gpu.sh [CONFIG_PATH] [VAL_DATA] [CHECKPOINT_PATH] (Optional)[PREDICTION_PATH]
```

## [Script Description](#contents)

### [Script and Sample Code](#contents)

```log
FSAF
├── README.md
├── __init__.py
├── configs
│   ├── default_config.yaml
│   ├── fsaf_r101_fpn_1x_coco.yaml
│   └── fsaf_r50_fpn_1x_coco.yaml
├── eval.py
├── infer.py
├── requirements.txt
├── scripts
│   ├── run_distribute_train_gpu.sh
│   ├── run_eval_gpu.sh
│   ├── run_infer_gpu.sh
│   └── run_standalone_train_gpu.sh
├── src
│   ├── __init__.py
│   ├── callback.py
│   ├── common.py
│   ├── dataset.py
│   ├── detecteval.py
│   ├── eval_utils.py
│   ├── fsaf
│   │   ├── __init__.py
│   │   ├── anchor_generator.py
│   │   ├── base_dense_head.py
│   │   ├── center_region_assigner.py
│   │   ├── conv_module.py
│   │   ├── focal_loss.py
│   │   ├── fpn.py
│   │   ├── fsaf.py
│   │   ├── fsaf_head.py
│   │   ├── initialization
│   │   │   ├── __init__.py
│   │   │   └── initialization.py
│   │   ├── iou_loss.py
│   │   ├── resnet.py
│   │   ├── tblr_bbox_coder.py
│   │   └── utils
│   │       ├── __init__.py
│   │       ├── misc.py
│   │       └── nms.py
│   ├── lr_schedule.py
│   ├── mlflow_funcs.py
│   ├── model_utils
│   │   ├── __init__.py
│   │   ├── config.py
│   │   ├── device_adapter.py
│   │   └── local_adapter.py
│   └── network_define.py
└── train.py
```

### [Script Parameters](#contents)

Major parameters in the yaml config file as follows:

```shell
train_outputs: './train_outputs'
brief: 'gpu-1'
device_target: GPU
# ==============================================================================
# backbone
backbone:
    type: 'ResNet'                                  ## Backbone type
    depth: 50                                       ## Backbone architecture
    num_stages: 4                                   ## Number of resnet/resnext stages
    out_indices: [0, 1, 2, 3]                       ## Define indices of resnet/resnext outputs
    frozen_stages: 1                                ## Number of frozen stages (use if backbone is pretrained, set -1 to not freeze)
    norm_eval: True                                 ## Whether make batch normalization work in eval mode all time (use if backbone is pretrained)
    pretrained: './checkpoints/resnet50.ckpt'       ## Path to pretrained backbone

neck:
  fpn:
    in_channels: [256, 512, 1024, 2048]                         ## Channels number for each input feature map (FPN)
    out_channels: 256                                           ## Channels number for each output feature map (FPN)
    start_level: 1
    add_extra_convs: 'on_input'
    num_outs: 5                                                 ## Number of output feature map (FPN)

# bbox head
bbox_head:
  num_classes: 80                                                 ## Number of classes
  in_channels: 256                                                ## Number of input channels
  stacked_convs: 4                                                ## Number of convolution layers per stage
  feat_channels: 256                                              ## Number of input channels
  bbox_coder:
    type: 'TBLRBBoxCoder'
    normalizer: 4.0
  loss_cls:
    type: 'FocalLoss'
    use_sigmoid: True
    gamma: 2.0                                                    ## Focal loss gamma parameter
    alpha: 0.25                                                   ## Focal loss alpha parameter
    loss_weight: 1.0                                              ## Classification loss weight
    reduction: 'none'
  loss_bbox:
    type: 'IoULoss'
    eps: 1e-06                                                   ## IoU Loss eps parameter
    loss_weight: 1.0                                             ## IoU Loss weight parameter
    reduction: 'none'                                            ## IoU Loss reduction parameter
  anchor_generator:
    type: 'AnchorGenerator'
    octave_base_scale: 1
    scales_per_octave: 1
    ratios: [1.0]
    strides: [8, 16, 32, 64, 128]

train_cfg:
  assigner:
    pos_scale: 0.2                                              ## IoU threshold for negative bboxes
    neg_scale: 0.2                                              ## IoU threshold for positive bboxes
    min_pos_iou: 0.01                                           ## Minimum iou for a bbox to be considered as a positive bbox

test_cfg:
  nms_pre: 1000                                                 ## max number of samples per level
  min_bbox_size: 0                                              ## min bboxes size
  score_thr: 0.05                                               ## Confidence threshold
  max_per_img: 100                                              ## Max number of output bboxes
  nms:
    iou_threshold: 0.5                                          ## IOU threshold

# optimizer
opt_type: 'sgd'                                                 ## Optimizer type (sgd or adam)
lr: 0.02                                                        ## Base learning rate
min_lr: 0.0000001                                               ## Minimum learning rate
momentum: 0.9                                                   ## Optimizer parameter
weight_decay: 0.0001                                            ## Regularization
warmup_step: 500                                                ## Number of warmup steps
warmup_ratio: 0.001                                             ## Initial learning rate = base_lr * warmup_ratio
lr_steps: [8, 11]                                               ## Epochs numbers when learning rate is divided by 10 (for multistep lr_type)
lr_type: 'multistep'                                            ## Learning rate scheduling type
grad_clip: 10                                                   ## Gradient clipping (set 0 to turn off)


# train
num_gts: 100                                                   ## Train batch size
batch_size: 4                                                  ## Train batch size
test_batch_size: 1                                             ## Test batch size
loss_scale: 256                                                ## Loss scale
epoch_size: 12                                                 ## Number of epochs
run_eval: 1                                                    ## Whether evaluation or not
eval_every: 1                                                  ## Evaluation interval
enable_graph_kernel: 0                                         ## Turn on kernel fusion
finetune: 0                                                    ## Turn on finetune (for transfer learning)
datasink: 0                                                    ## Turn on data sink mode
pre_trained: ''                                                ## Path to pretrained model weights

#distribution training
run_distribute: 0                                              ## Turn on distributed training
device_id: 0                                                   ##
device_num: 1                                                  ## Number of devices (only if distributed training turned on)
rank_id: 0                                                     ##

# Number of threads used to process the dataset in parallel
num_parallel_workers: 6
# Parallelize Python operations with multiple worker processes
python_multiprocessing: 0
# dataset setting
train_dataset: '/data/coco-2017/train/'
val_dataset: '/data/coco-2017/validation/'
coco_classes: ['background', 'person', 'bicycle', 'car', 'motorcycle',
               'airplane', 'bus', 'train', 'truck', 'boat', 'traffic light',
               'fire hydrant', 'stop sign', 'parking meter', 'bench', 'bird',
               'cat', 'dog', 'horse', 'sheep', 'cow', 'elephant', 'bear',
               'zebra', 'giraffe', 'backpack', 'umbrella', 'handbag', 'tie',
               'suitcase', 'frisbee', 'skis', 'snowboard', 'sports ball',
               'kite', 'baseball bat', 'baseball glove', 'skateboard',
               'surfboard', 'tennis racket', 'bottle', 'wine glass', 'cup',
               'fork', 'knife', 'spoon', 'bowl', 'banana', 'apple',
               'sandwich', 'orange', 'broccoli', 'carrot', 'hot dog', 'pizza',
               'donut', 'cake', 'chair', 'couch', 'potted plant', 'bed',
               'dining table', 'toilet', 'tv', 'laptop', 'mouse', 'remote',
               'keyboard', 'cell phone', 'microwave', 'oven', 'toaster',
               'sink', 'refrigerator', 'book', 'clock', 'vase', 'scissors',
               'teddy bear', 'hair drier', 'toothbrush']
num_classes: 80
train_dataset_num: 0
train_dataset_divider: 0

# images
img_width: 1024                                                         ## Input images width
img_height: 1024                                                        ## Input images height
divider: 64                                                             ## Automatically make width and height are dividable by divider
img_mean: [123.675, 116.28, 103.53]                                     ## Image normalization parameters
img_std: [58.395, 57.12, 57.375]                                        ## Image normalization parameters
to_rgb: 1                                                               ## RGB or BGR
keep_ratio: 1                                                           ## Keep ratio in original images

# augmentation
flip_ratio: 0.5                                                         ## Probability of image horizontal flip
expand_ratio: 0.0                                                       ## Probability of image expansion

# callbacks
save_every: 100                                                         ## Save model every <n> steps
keep_checkpoint_max: 5                                                  ## Max number of saved periodical checkpoints
keep_best_checkpoints_max: 5                                            ## Max number of saved best checkpoints
print_loss_every: 1                                                     ## Print loss every N steps
 ```

## [Training](#contents)

To train the model, run `train.py`.

### [Training process](#contents)

Standalone training mode:

```bash
bash scripts/run_standalone_train_gpu.sh [CONFIG_PATH] [TRAIN_DATA] [VAL_DATA] [TRAIN_OUT] [BRIEF] (OPTIONAL)[PRETRAINED_PATH]
```

We need several parameters for these scripts.

* `CONFIG_PATH`: path to config file.
* `TRAIN_DATA`: path to train dataset.
* `VAL_DATA`: path to validation dataset.
* `TRAIN_OUT`: path to folder with training experiments.
* `BRIEF`: short experiment name.
* `PRETRAINED_PATH`: the path of pretrained checkpoint file, it is better
 to use absolute path.

### [Transfer Training](#contents)

You can train your own model based on either pretrained classification model
or pretrained detection model. You can perform transfer training by following
steps.

1. Prepare your dataset.
2. Change configuration YAML file according to your own dataset, especially
 the change `num_classes` value and `coco_classes` list.
3. Prepare a pretrained checkpoint. You can load the pretrained checkpoint by
 `pretrained` argument. Transfer training means a new training job, so just set
 `finetune` 1.
4. Run training.

### [Distribute training](#contents)

Distribute training mode (OpenMPI must be installed):

```shell
bash scripts/run_distribute_train_gpu.sh [CONFIG_PATH] [DEVICE_NUM] [TRAIN_DATA] [VAL_DATA] [TRAIN_OUT] [BRIEF] (OPTIONAL)[PRETRAINED_PATH]
```

We need several parameters for these scripts:

* `CONFIG_PATH`: path to config file.
* `DEVICE_NUM`: number of devices.
* `TRAIN_DATA`: path to train dataset.
* `VAL_DATA`: path to validation dataset.
* `TRAIN_OUT`: path to folder with training experiments.
* `BRIEF`: short experiment name.
* `PRETRAINED_PATH`: the path of pretrained checkpoint file, it is better
 to use absolute path.

## [Evaluation](#contents)

### [Evaluation process](#contents)

#### [Evaluation on GPU](#contents)

```shell
bash scripts/run_eval_gpu.sh [CONFIG_PATH] [VAL_DATA] [CHECKPOINT_PATH] (Optional)[PREDICTION_PATH]
```

We need four parameters for this script.

* `CONFIG_PATH`: path to config file.
* `VAL_DATA`: the absolute path for dataset subset (validation).
* `CHECKPOINT_PATH`: path to checkpoint.
* `PREDICTION_PATH`: path to file with predictions JSON file (predictions may
 be saved to this file and loaded after).

> checkpoint can be produced in training process.

### [Evaluation result](#contents)

Result for GPU:

```log
total images num:  5000
Processing, please wait a moment.
100%|██████████| 5000/5000 [12:56<00:00,  6.44it/s]
Loading and preparing results...
DONE (t=0.98s)
creating index...
index created!
Running per image evaluation...
Evaluate annotation type *bbox*
DONE (t=34.59s).
Accumulating evaluation results...
DONE (t=5.76s).
 Average Precision  (AP) @[ IoU=0.50:0.95 | area=   all | maxDets=100 ] = 0.369
 Average Precision  (AP) @[ IoU=0.50      | area=   all | maxDets=100 ] = 0.563
 Average Precision  (AP) @[ IoU=0.75      | area=   all | maxDets=100 ] = 0.392
 Average Precision  (AP) @[ IoU=0.50:0.95 | area= small | maxDets=100 ] = 0.197
 Average Precision  (AP) @[ IoU=0.50:0.95 | area=medium | maxDets=100 ] = 0.402
 Average Precision  (AP) @[ IoU=0.50:0.95 | area= large | maxDets=100 ] = 0.492
 Average Recall     (AR) @[ IoU=0.50:0.95 | area=   all | maxDets=  1 ] = 0.315
 Average Recall     (AR) @[ IoU=0.50:0.95 | area=   all | maxDets= 10 ] = 0.508
 Average Recall     (AR) @[ IoU=0.50:0.95 | area=   all | maxDets=100 ] = 0.538
 Average Recall     (AR) @[ IoU=0.50:0.95 | area= small | maxDets=100 ] = 0.330
 Average Recall     (AR) @[ IoU=0.50:0.95 | area=medium | maxDets=100 ] = 0.582
 Average Recall     (AR) @[ IoU=0.50:0.95 | area= large | maxDets=100 ] = 0.690
Eval result: 0.36865292009957495

Evaluation done!

Done!
Time taken: 826 seconds
```

## [Inference](#contents)

### [Inference process](#contents)

#### [Inference on GPU](#contents)

Run model inference from retinanet directory:

```bash
bash scripts/run_infer_gpu.sh [CONFIG_PATH] [CHECKPOINT_PATH] [PRED_INPUT] [PRED_OUTPUT]
```

We need 4 parameters for these scripts:

* `CONFIG_FILE`： path to config file.
* `CHECKPOINT_PATH`: path to saved checkpoint.
* `PRED_INPUT`: path to input folder or image.
* `PRED_OUTPUT`: path to output JSON file.

### [Inference result](#contents)

Predictions will be saved in JSON file. File content is list of predictions
for each image. It's supported predictions for folder of images (png, jpeg
file in folder root) and single image.

## [Model Description](#contents)

### [Performance](#contents)

| Parameters          | GPU                                     |
|---------------------|-----------------------------------------|
| Model Version       | FSAF ResNet 50 1024x1024                |
| Resource            | NVIDIA TitanX (x2)                      |
| Uploaded Date       | 19/10/2023 (day/month/year)             |
| MindSpore Version   | 1.10.0                                  |
| Dataset             | COCO2017                                |
| Pretrained          | noised checkpoint (mAP=35.6%)           |
| Training Parameters | epoch = 12, batch_size = 1              |
| Optimizer           | SGD (momentum)                          |
| Loss Function       | SigmoidFocalClassificationLoss, IoULoss |
| Speed               | 4pcs: 461.8 ms/step                     |
| Total time          | 4pcs: 7h 31m 18s                        |
| outputs             | mAP                                     |
| mAP                 | 36.34%                                  |
| Model for inference | 129.9M(.ckpt file)                      |
| configuration       | fsaf_r50_fpn_1x_coco.yaml               |
| Scripts             |                                         |

## [Description of Random Situation](#contents)

In dataset.py, we set the seed inside "create_dataset" function. We also use
random seed in train.py.

## [ModelZoo Homepage](#contents)

Please check the official [homepage](https://gitee.com/mindspore/models).
