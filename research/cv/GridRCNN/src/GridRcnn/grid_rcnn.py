# Copyright 2022-2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
"""GridRcnn"""
import logging
import os

import numpy as np
import mindspore as ms
import mindspore.ops as ops
import mindspore.nn as nn
from mindspore.common.tensor import Tensor
from .bbox_assign_sample_stage2 import BboxAssignSampleForRcnn
from .fpn_neck import FeatPyramidNeck
from .proposal_generator import Proposal
from .rpn import RPN
from .roi_align import SingleRoIExtractor
from .anchor_generator import AnchorGenerator
from .bbox_head_grid import BBoxHeadGrid
from .grid_head import GridHead
from .resnet import ResNet
from .resnext import ResNeXt


class Grid_Rcnn(nn.Cell):
    """
    GridRcnn Network.

    Note:
        backbone = config.backbone

    Returns:
        Tuple, tuple of output tensor.
        rpn_loss: Scalar, Total loss of RPN subnet.
        rcnn_cls_loss: Scalar, Classification loss of BboxHeadGrid subnet.
        grid_loss: Scalar, Grid loss loss of GridHead subnet
        rpn_cls_loss: Scalar, Classification loss of RPN subnet.
        rpn_reg_loss: Scalar, Regression loss of RPN subnet.

    Examples:
        net = Grid_Rcnn(config)
    """
    def __init__(self, config):
        super(Grid_Rcnn, self).__init__()
        self.dtype = np.float32
        self.ms_type = ms.float32
        self.train_batch_size = config.batch_size
        self.test_batch_size = config.test_batch_size
        self.num_classes = config.num_classes
        self.num_cls_bbox = config.num_classes
        self.anchor_scales = config.anchor_scales
        self.anchor_ratios = config.anchor_ratios
        self.anchor_strides = config.anchor_strides
        self.max_num = config.num_gts
        self.zero_tensor = ms.Tensor(0.)
        self.random_jitter_amplitude = ms.Tensor(0.15)
        self.grid_max_num = config.grid_max_num

        # Anchor generator
        self.init_anchor(config)

        # Backbone
        self.init_backbone(config)

        # Fpn
        self.neck = FeatPyramidNeck(
            config.fpn_in_channels, config.fpn_out_channels,
            config.fpn_num_outs, config.feature_shapes
        )

        # Rpn and rpn loss
        self.gt_labels_stage1 = Tensor(
            np.ones((self.train_batch_size, config.num_gts)).astype(np.uint8)
        )
        self.rpn_head = RPN(
            config, self.train_batch_size, config.rpn_in_channels,
            config.rpn_feat_channels, config.num_anchors,
            config.rpn_cls_out_channels
        )

        # Proposal
        (
            self.proposal_generator, self.proposal_generator_test
        ) = self.create_proposal_generator(config)

        # Assign and sampler stage two
        self.bbox_assigner_sampler_for_rcnn = BboxAssignSampleForRcnn(
            config, self.train_batch_size, config.num_bboxes_stage2, True
        )
        # Roi
        self.roi_align, self.roi_align_test = self.create_roi(
            roi_layer=config.roi_layer,
            roi_align_featmap_strides=config.roi_align_featmap_strides,
            roi_align_out_channels=config.roi_align_out_channels,
            roi_align_finest_scale=config.roi_align_finest_scale,
            train_batch_size=config.roi_sample_num * self.train_batch_size,
            test_batch_size=config.rpn_max_num * self.test_batch_size,
        )

        self.bbox_head = BBoxHeadGrid(
            config, num_fcs=config.rcnn_num_fcs,
            roi_feat_size=config.roi_layer.out_size,
            in_channels=config.rcnn_in_channels, num_classes=self.num_classes,
            fc_out_channels=config.rcnn_fc_out_channels,
            loss_cls_weight=config.rcnn_loss_cls_weight
        )

        self.grid_roi_align, self.grid_roi_align_test = self.create_roi(
            roi_layer=config.grid_roi_layer,
            roi_align_featmap_strides=config.grid_roi_align_featmap_strides,
            roi_align_out_channels=config.grid_roi_align_out_channels,
            roi_align_finest_scale=config.grid_roi_align_finest_scale,
            train_batch_size=(
                config.grid_roi_sample_num * self.train_batch_size
            ),
            test_batch_size=self.max_num * self.test_batch_size,
        )

        # Grid
        self.grid_head = GridHead(
            config, num_convs=config.grid_num_convs,
            in_channels=config.grid_in_channels,
            conv_out_channels=config.grid_conv_out_channels,
            num_grids=config.num_grids, mask_size=config.grid_mask_size,
            roi_feat_size=config.grid_roi_layer.out_size,
            loss_weight=config.grid_loss_weight
        )

        # Op declare
        self.init_ops()

        # Improve speed
        self.concat_start = min(self.num_classes - 2, 55)
        self.concat_end = (self.num_classes - 1)

        # Test mode
        self.test_mode_init(config)

        # Init tensor
        self.init_tensor(config)

    def init_anchor(self, config):
        """Init anchor generators and anchors."""
        anchor_base_sizes = None
        self.anchor_base_sizes = (
            list(self.anchor_strides) if anchor_base_sizes is None
            else anchor_base_sizes
        )

        self.anchor_generators = []
        for anchor_base in self.anchor_base_sizes:
            self.anchor_generators.append(
                AnchorGenerator(anchor_base, self.anchor_scales,
                                self.anchor_ratios)
            )

        self.num_anchors = len(self.anchor_ratios) * len(self.anchor_scales)

        featmap_sizes = config.feature_shapes
        assert len(featmap_sizes) == len(self.anchor_generators)
        self.anchor_list = self.get_anchors(featmap_sizes)

    def get_anchors(self, featmap_sizes):
        """Get anchors according to feature map sizes.

        Args:
            featmap_sizes (list[tuple]): Multi-level feature map sizes.

        Returns:
            tuple: anchors of each image, valid flags of each image
        """
        num_levels = len(featmap_sizes)

        # since feature map sizes of all images are the same, we only compute
        # anchors for one time
        multi_level_anchors = ()
        for i in range(num_levels):
            anchors = self.anchor_generators[i].grid_anchors(
                featmap_sizes[i], self.anchor_strides[i])
            multi_level_anchors += (Tensor(anchors.astype(self.dtype)),)

        return multi_level_anchors

    def init_ops(self):
        """Init operations used in calculation."""
        self.squeeze = ops.Squeeze()
        self.cast = ops.Cast()

        self.concat = ops.Concat(axis=0)
        self.concat_1 = ops.Concat(axis=1)
        self.concat_2 = ops.Concat(axis=2)
        self.reshape = ops.Reshape()
        self.select = ops.Select()
        self.greater = ops.Greater()
        self.transpose = ops.Transpose()

    def test_mode_init(self, config):
        """
        Initialize test_mode from the config file.

        Args:
            config (file): config file.

        Examples:
            self.test_mode_init(config)
        """
        self.split = ops.Split(axis=0, output_num=self.test_batch_size)
        self.split_shape = ops.Split(axis=0, output_num=4)
        self.split_scores = ops.Split(axis=1, output_num=self.num_classes)
        self.split_cls = ops.Split(axis=0, output_num=self.num_classes-1)
        self.tile = ops.Tile()
        self.gather = ops.GatherNd()

        self.rpn_max_num = config.rpn_max_num

        self.zeros_for_nms = Tensor(
            np.zeros((self.rpn_max_num, 3)).astype(self.dtype)
        )
        self.ones_mask = np.ones((self.rpn_max_num, 1)).astype(np.bool)
        self.zeros_mask = np.zeros((self.rpn_max_num, 1)).astype(np.bool)
        self.bbox_mask = Tensor(
            np.concatenate(
                (self.ones_mask, self.zeros_mask, self.ones_mask,
                 self.zeros_mask),
                axis=1
            )
        )
        self.nms_pad_mask = Tensor(np.concatenate((self.ones_mask, self.ones_mask,
                                                   self.ones_mask, self.ones_mask, self.zeros_mask), axis=1))

        self.test_score_thresh = Tensor(np.ones((self.rpn_max_num, 1)).astype(self.dtype) * config.test_score_thr)
        self.test_score_zeros = Tensor(np.ones((self.rpn_max_num, 1)).astype(self.dtype) * 0)
        self.test_box_zeros = Tensor(np.ones((self.rpn_max_num, 4)).astype(self.dtype) * -1)
        self.test_iou_thr = Tensor(np.ones((self.rpn_max_num, 1)).astype(self.dtype) * config.test_iou_thr)
        self.nms_test = ops.NMSWithMask(config.test_iou_thr)
        self.softmax = ops.Softmax(axis=1)
        self.logicand = ops.LogicalAnd()
        self.oneslike = ops.OnesLike()
        self.test_topk = ops.TopK(sorted=True)
        self.test_num_proposal = self.test_batch_size * self.rpn_max_num

    def init_backbone(self, config):
        """Create backbone and init it."""
        if config.backbone_type == 'resnet':
            self.backbone = ResNet(
                depth=config.resnet_depth,
                num_stages=config.resnet_num_stages,
                strides=(1, 2, 2, 2),
                dilations=(1, 1, 1, 1),
                out_indices=config.resnet_out_indices,
                frozen_stages=config.resnet_frozen_stages,
                norm_eval=config.resnet_norm_eval
            )
        elif config.backbone_type == 'resnext':
            self.backbone = ResNeXt(
                depth=config.resnet_depth,
                num_stages=config.resnet_num_stages,
                strides=(1, 2, 2, 2),
                dilations=(1, 1, 1, 1),
                out_indices=config.resnet_out_indices,
                frozen_stages=config.resnet_frozen_stages,
                norm_eval=config.resnet_norm_eval,
                groups=config.resnext_groups,
                base_width=config.resnext_base_width
            )
        else:
            raise ValueError(f'Unsupported backbone: {config.backbone_type}')
        if (config.backbone_pretrained is not None
                and os.path.exists(config.backbone_pretrained)):
            logging.info('Load backbone weights...')
            ms.load_checkpoint(config.backbone_pretrained, self.backbone)

    def create_proposal_generator(self, config):
        """Create proposal generator."""
        proposal_generator = Proposal(
            config, self.train_batch_size, config.activate_num_classes,
            config.use_sigmoid_cls
        )
        proposal_generator.set_train_local(config, True)
        proposal_generator_test = Proposal(
            config, config.test_batch_size, config.activate_num_classes,
            config.use_sigmoid_cls
        )
        proposal_generator_test.set_train_local(config, False)

        return proposal_generator, proposal_generator_test

    def create_roi(
            self, roi_layer, roi_align_out_channels, roi_align_featmap_strides,
            roi_align_finest_scale, train_batch_size, test_batch_size
    ):
        """Create roi extraction blocks in training and inference mode."""
        roi_align = SingleRoIExtractor(
            roi_layer=roi_layer,
            out_channels=roi_align_out_channels,
            featmap_strides=roi_align_featmap_strides,
            train_batch_size=train_batch_size,
            test_batch_size=test_batch_size,
            finest_scale=roi_align_finest_scale
        )
        roi_align.set_train_local(True)
        roi_align_test = SingleRoIExtractor(
            roi_layer=roi_layer,
            out_channels=roi_align_out_channels,
            featmap_strides=roi_align_featmap_strides,
            train_batch_size=train_batch_size,
            test_batch_size=test_batch_size,
            finest_scale=roi_align_finest_scale
        )
        roi_align_test.set_train_local(False)

        return roi_align, roi_align_test

    def init_tensor(self, config):
        """Init some helpful tensors."""
        num_expected_total = (
            config.num_expected_pos_stage2 + config.num_expected_neg_stage2
        )
        roi_align_index = [
            np.array(
                np.ones((num_expected_total, 1)) * i, dtype=self.dtype
            ) for i in range(self.train_batch_size)
        ]
        grid_roi_align_index = [
            np.array(
                np.ones((config.num_expected_pos_stage2, 1)) * i,
                dtype=self.dtype
            ) for i in range(self.train_batch_size)
        ]

        roi_align_index_test = [
            np.array(np.ones((config.rpn_max_num, 1)) * i, dtype=self.dtype)
            for i in range(self.test_batch_size)
        ]
        grid_roi_align_index_test = [
            np.array(np.ones((self.max_num, 1)) * i, dtype=self.dtype)
            for i in range(self.test_batch_size)
        ]

        self.roi_align_index_tensor = Tensor(np.concatenate(roi_align_index))
        self.grid_roi_align_index_tensor = Tensor(
            np.concatenate(grid_roi_align_index)
        )
        self.roi_align_index_test_tensor = Tensor(
            np.concatenate(roi_align_index_test)
        )
        self.grid_roi_align_index_test_tensor = Tensor(
            np.concatenate(grid_roi_align_index_test)
        )

    def construct(self, img_data, img_metas, gt_bboxes, gt_labels, gt_valids):
        """
        construct the GridRcnn Network.

        Args:
            img_data: input image data.
            img_metas: meta label of img.
            gt_bboxes (Tensor): get the value of bboxes.
            gt_labels (Tensor): get the value of labels.
            gt_valids (Tensor): get the valid part of bboxes.

        Returns:
            Tuple,tuple of output tensor
        """
        x = self.backbone(img_data)
        x = self.neck(x)
        if self.training:
            output = self.run_train(
                x, img_metas, gt_bboxes, gt_labels, gt_valids
            )
        else:
            output = self.run_test(x, img_metas)

        return output

    def run_train(self, feats, img_metas, gt_bboxes, gt_labels, gt_valids):
        """Run GridRcnn loss calculation."""
        rpn_loss, cls_score, bbox_pred, rpn_cls_loss, rpn_reg_loss, _ = (
            self.rpn_head(
                feats, img_metas, self.anchor_list, gt_bboxes,
                self.gt_labels_stage1, gt_valids
            )
        )
        proposal, proposal_mask = self.proposal_generator(
            cls_score, bbox_pred, self.anchor_list
        )
        proposal = tuple([ops.stop_gradient(p) for p in proposal])
        proposal_mask = tuple([ops.stop_gradient(p) for p in proposal_mask])

        gt_labels = self.cast(gt_labels, ms.int32)
        gt_valids = self.cast(gt_valids, ms.int32)

        (
            bboxes_tuple, labels_tuple, mask_tuple, pos_tuple, pos_gt_tuple,
            pos_mask_tuple
        ) = self.assign_sample(
            gt_bboxes, gt_labels, gt_valids, proposal, proposal_mask
        )

        rcnn_labels = self.concat(labels_tuple)
        rcnn_labels = ops.stop_gradient(rcnn_labels)
        rcnn_labels = self.cast(rcnn_labels, ms.int32)

        if self.train_batch_size > 1:
            bboxes_all = self.concat(bboxes_tuple)
        else:
            bboxes_all = bboxes_tuple[0]

        rois = self.concat_1((self.roi_align_index_tensor, bboxes_all))
        rois = self.cast(rois, ms.float32)
        rois = ops.stop_gradient(rois)

        roi_feats = self.roi_align(rois,
                                   self.cast(feats[0], ms.float32),
                                   self.cast(feats[1], ms.float32),
                                   self.cast(feats[2], ms.float32),
                                   self.cast(feats[3], ms.float32))

        roi_feats = self.cast(roi_feats, self.ms_type)
        rcnn_masks = self.concat(mask_tuple)
        rcnn_masks = ops.stop_gradient(rcnn_masks)
        rcnn_mask_squeeze = self.squeeze(self.cast(rcnn_masks, ms.bool_))

        pos_masks = self.concat(pos_mask_tuple)
        pos_masks = ops.stop_gradient(pos_masks)
        pos_masks_squeeze = self.squeeze(self.cast(pos_masks, ms.bool_))

        rcnn_cls_loss = self.bbox_head(
            roi_feats, rcnn_labels, rcnn_mask_squeeze
        )

        jit_pos = self.random_jitter(pos_tuple, img_metas)
        if self.train_batch_size > 1:
            pos_all = self.concat(jit_pos)
            pos_gt_all = self.concat(pos_gt_tuple)
        else:
            pos_all = jit_pos[0]
            pos_gt_all = pos_gt_tuple[0]
        grid_rois = self.concat_1(
            (self.grid_roi_align_index_tensor, pos_all)
        )

        grid_rois = self.cast(grid_rois, self.ms_type)
        grid_rois = ops.stop_gradient(grid_rois)

        grid_roi_feats = self.grid_roi_align(
            grid_rois,
            self.cast(feats[0], self.ms_type),
            self.cast(feats[1], self.ms_type),
            self.cast(feats[2], self.ms_type),
            self.cast(feats[3], self.ms_type)
        )
        grid_roi_feats = self.cast(grid_roi_feats, self.ms_type)

        grid_targets = self.grid_head.get_target(
            pos_bboxes=pos_all, pos_gt_bboxes=pos_gt_all
        )
        grid_targets = ops.stop_gradient(grid_targets)

        grid_loss = self.grid_head(
            grid_roi_feats, grid_targets, pos_masks_squeeze
        )

        return (
            rpn_loss, rcnn_cls_loss, grid_loss, rpn_cls_loss, rpn_reg_loss
        )

    def assign_sample(
            self, gt_bboxes, gt_labels, gt_valids, proposal, proposal_mask
    ):
        """Prepare proposed samples for training."""
        bboxes_tuple = ()
        labels_tuple = ()
        mask_tuple = ()
        pos_tuple = ()
        pos_gt_tuple = ()
        pos_mask_tuple = ()
        for i in range(self.train_batch_size):
            gt_bboxes_i = self.squeeze(gt_bboxes[i:i + 1:1, ::])
            gt_labels_i = self.squeeze(gt_labels[i:i + 1:1, ::])
            gt_labels_i = self.cast(gt_labels_i, ms.uint8)
            gt_valids_i = self.squeeze(gt_valids[i:i + 1:1, ::])
            gt_valids_i = self.cast(gt_valids_i, ms.bool_)

            (
                bboxes, labels, mask, pos_bboxes, pos_gt_bboxes, pos_mask
            ) = (
                self.bbox_assigner_sampler_for_rcnn(
                    gt_bboxes_i, gt_labels_i, proposal_mask[i],
                    proposal[i][::, 0:4:1], gt_valids_i
                )
            )

            bboxes_tuple += (bboxes,)
            labels_tuple += (labels,)
            mask_tuple += (mask,)
            pos_tuple += (pos_bboxes,)
            pos_gt_tuple += (pos_gt_bboxes,)
            pos_mask_tuple += (pos_mask,)

        return (
            bboxes_tuple, labels_tuple, mask_tuple, pos_tuple, pos_gt_tuple,
            pos_mask_tuple
        )

    def run_test(self, feats, img_metas):
        """Run prediction calculation."""
        _, cls_score, bbox_pred, _, _, _ = (
            self.rpn_head(
                feats, img_metas, self.anchor_list, None,
                self.gt_labels_stage1, None
            )
        )
        proposal, proposal_mask = self.proposal_generator_test(
            cls_score, bbox_pred, self.anchor_list
        )
        bboxes_tuple = ()
        mask_tuple = ()

        mask_tuple += proposal_mask
        for p_i in proposal:
            bboxes_tuple += (p_i[::, 0:4:1],)

        if self.test_batch_size > 1:
            bboxes_all = self.concat(bboxes_tuple)
        else:
            bboxes_all = bboxes_tuple[0]

        rois = self.concat_1(
            (self.roi_align_index_test_tensor, bboxes_all)
        )
        rois = self.cast(rois, ms.float32)
        rois = ops.stop_gradient(rois)
        roi_feats = self.roi_align_test(
            rois,
            self.cast(feats[0], ms.float32),
            self.cast(feats[1], ms.float32),
            self.cast(feats[2], ms.float32),
            self.cast(feats[3], ms.float32)
        )

        roi_feats = self.cast(roi_feats, self.ms_type)
        rcnn_masks = self.concat(mask_tuple)
        rcnn_masks = ops.stop_gradient(rcnn_masks)
        # rcnn_mask_squeeze = self.squeeze(self.cast(rcnn_masks, ms.bool_))

        rcnn_out1 = self.bbox_head(
            roi_feats, None, None
        )

        res_bboxes, res_scores, res_mask = self.get_det_bboxes(
            rcnn_out1, rcnn_masks, bboxes_all, img_metas
        )
        res_bboxes_reshaped = res_bboxes.reshape((-1, 5))
        grid_rois = self.concat_1(
            (
                self.grid_roi_align_index_test_tensor,
                res_bboxes_reshaped[..., :4]
            )
        )
        grid_rois = self.cast(grid_rois, ms.float32)
        grid_rois = ops.stop_gradient(grid_rois)
        grid_roi_feats = self.grid_roi_align_test(
            grid_rois,
            self.cast(feats[0], self.ms_type),
            self.cast(feats[1], self.ms_type),
            self.cast(feats[2], self.ms_type),
            self.cast(feats[3], self.ms_type)
        )
        grid_roi_feats = self.cast(grid_roi_feats, self.ms_type)
        grid_pred = self.grid_head(grid_roi_feats, None, None)

        res_bboxes_grid = self.grid_head.get_bboxes(
            res_bboxes_reshaped, grid_pred, img_metas
        ).reshape((self.test_batch_size, self.max_num, 5))

        return res_bboxes_grid, res_scores, res_mask, None, None

    def get_det_bboxes(
            self, cls_logits, mask_logits, rois, img_metas
    ):
        """Get the actual detection box."""
        scores = self.softmax(cls_logits)
        boxes_all = tuple([rois for i in range(self.num_cls_bbox)])

        scores_all = self.split(scores)
        mask_all = self.split(self.cast(mask_logits, ms.int32))

        boxes_all_with_batchsize = ()
        for i in range(self.test_batch_size):
            boxes_tuple = ()
            for j in range(self.num_cls_bbox):

                boxes_tuple += self.split(boxes_all[j])
            boxes_all_with_batchsize += (boxes_tuple,)

        res_bboxes_nms, res_scores_nms, res_mask_nms = self.multiclass_nms(
            boxes_all_with_batchsize, scores_all, mask_all
        )
        res_bboxes = ()
        res_scores = ()
        res_mask = ()
        for i in range(self.test_batch_size):
            res_bboxes_, res_scores_, res_mask_ = self.get_best(
                res_bboxes_nms[i], res_scores_nms[i], res_mask_nms[i]
            )
            res_bboxes += (res_bboxes_,)
            res_scores += (res_scores_,)
            res_mask += (res_mask_,)

        res_bboxes = self.concat(res_bboxes).reshape((-1, self.max_num, 5))
        res_scores = self.concat(res_scores).reshape((-1, self.max_num, 1))
        res_mask = self.concat(res_mask).reshape((-1, self.max_num, 1))
        return res_bboxes, res_scores, res_mask

    def multiclass_nms(self, boxes_all, scores_all, mask_all):
        """Multiscale postprocessing."""
        all_bboxes = ()
        all_labels = ()
        all_masks = ()

        for i in range(self.test_batch_size):
            bboxes = boxes_all[i]
            scores = scores_all[i]
            masks = self.cast(mask_all[i], ms.bool_)

            res_boxes_tuple = ()
            res_labels_tuple = ()
            res_masks_tuple = ()

            for j in range(self.num_classes - 1):
                k = j + 1
                _cls_scores = scores[::, k:k + 1:1]
                _bboxes = self.squeeze(bboxes[k])
                _mask_o = self.reshape(masks, (self.rpn_max_num, 1))

                cls_mask = self.greater(_cls_scores, self.test_score_thresh)
                _mask = self.logicand(_mask_o, cls_mask)

                _reg_mask = self.cast(
                    self.tile(self.cast(_mask, ms.int32), (1, 4)), ms.bool_
                )

                _bboxes = self.select(_reg_mask, _bboxes, self.test_box_zeros)
                _cls_scores = self.select(
                    _mask, _cls_scores, self.test_score_zeros
                )
                __cls_scores = self.squeeze(_cls_scores)
                scores_sorted, topk_inds = self.test_topk(
                    __cls_scores, self.rpn_max_num
                )
                topk_inds = self.reshape(topk_inds, (self.rpn_max_num, 1))
                scores_sorted = self.reshape(
                    scores_sorted, (self.rpn_max_num, 1)
                )
                _bboxes_sorted = self.gather(_bboxes, topk_inds)
                _mask_sorted = self.gather(_mask, topk_inds)

                scores_sorted = self.tile(scores_sorted, (1, 4))
                cls_dets = self.concat_1((_bboxes_sorted, scores_sorted))
                cls_dets = ops.Slice()(cls_dets, (0, 0), (self.rpn_max_num, 5))

                cls_dets, _index, _mask_nms = self.nms_test(cls_dets)
                cls_dets, _index, _mask_nms = [
                    ms.ops.stop_gradient(a)
                    for a in (cls_dets, _index, _mask_nms)
                ]
                # print('mask_nms', _mask_nms.sum())
                _index = self.reshape(_index, (self.rpn_max_num, 1))
                _mask_nms = self.reshape(_mask_nms, (self.rpn_max_num, 1))

                _mask_n = self.gather(_mask_sorted, _index)
                _mask_n = self.logicand(_mask_n, _mask_nms)
                cls_labels = self.oneslike(_index) * j
                res_boxes_tuple += (cls_dets,)
                res_labels_tuple += (cls_labels,)
                res_masks_tuple += (_mask_n,)

            res_boxes_start = self.concat(res_boxes_tuple[:self.concat_start])
            res_labels_start = self.concat(
                res_labels_tuple[:self.concat_start]
            )
            res_masks_start = self.concat(res_masks_tuple[:self.concat_start])

            res_boxes_end = self.concat(
                res_boxes_tuple[self.concat_start:self.concat_end]
            )
            res_labels_end = self.concat(
                res_labels_tuple[self.concat_start:self.concat_end]
            )
            res_masks_end = self.concat(
                res_masks_tuple[self.concat_start:self.concat_end]
            )

            res_boxes = self.concat((res_boxes_start, res_boxes_end))
            res_labels = self.concat((res_labels_start, res_labels_end))
            res_masks = self.concat((res_masks_start, res_masks_end))

            reshape_size = (self.num_classes - 1) * self.rpn_max_num
            res_boxes = self.reshape(res_boxes, (1, reshape_size, 5))
            res_labels = self.reshape(res_labels, (1, reshape_size, 1))
            res_masks = self.reshape(res_masks, (1, reshape_size, 1))

            all_bboxes += (res_boxes,)
            all_labels += (res_labels,)
            all_masks += (res_masks,)

        all_bboxes = self.concat(all_bboxes)
        all_labels = self.concat(all_labels)
        all_masks = self.concat(all_masks)
        return all_bboxes, all_labels, all_masks

    def get_best(self, bboxes, labels, masks):
        """Filter predicted bboxes by score."""
        score = bboxes[::, 4] * masks.reshape(-1)
        _, score_indicies = self.test_topk(score, self.max_num)
        # score_indicies = score_indicies[:self.max_num]
        bboxes = bboxes[score_indicies]
        labels = labels[score_indicies]
        masks = masks[score_indicies]

        return bboxes, labels, masks

    def random_jitter(self, pos_boxes, img_metas):
        """Make random change of boxes sizes and positions."""
        jitter_results = ()
        for i in range(self.train_batch_size):
            jitter_results += (
                self.random_jitter_single(pos_boxes[i], img_metas[i]),
            )
        return jitter_results

    def random_jitter_single(self, pos_boxes, img_meta):
        """Make random change of boxes sizes and positions."""
        n, _ = pos_boxes.shape
        random_offset = ms.ops.uniform(
            shape=(n, 4), minval=-self.random_jitter_amplitude,
            maxval=self.random_jitter_amplitude
        )
        # before jittering
        ctx_ = (pos_boxes[:, 2] + pos_boxes[:, 0]) / 2
        cty_ = (pos_boxes[:, 1] + pos_boxes[:, 3]) / 2
        width_ = (pos_boxes[:, 2] - pos_boxes[:, 0]).abs()
        height_ = (pos_boxes[:, 3] - pos_boxes[:, 1]).abs()
        # after jittering
        ctx = ctx_ + random_offset[:, 0] * width_
        cty = cty_ + random_offset[:, 1] * height_
        width = width_ * (1 + random_offset[:, 2])
        height = height_ * (1 + random_offset[:, 3])

        x1 = (ctx - width / 2).view(-1, 1)
        y1 = (cty - height / 2).view(-1, 1)
        x2 = (ctx + width / 2).view(-1, 1)
        y2 = (cty + height / 2).view(-1, 1)

        max_shape = img_meta[:2]
        x1 = ms.ops.clip_by_value(
            x1, clip_value_min=self.zero_tensor,
            clip_value_max=max_shape[1] - 1
        )
        y1 = ms.ops.clip_by_value(
            y1, clip_value_min=self.zero_tensor,
            clip_value_max=max_shape[0] - 1
        )
        x2 = ms.ops.clip_by_value(
            x2, clip_value_min=self.zero_tensor,
            clip_value_max=max_shape[1] - 1
        )
        y2 = ms.ops.clip_by_value(
            y2, clip_value_min=self.zero_tensor,
            clip_value_max=max_shape[0] - 1
        )

        res = self.concat_1((x1, y1, x2, y2))
        return res

    def set_train(self, mode=True):
        """Change training mode."""
        super(Grid_Rcnn, self).set_train(mode=mode)
        self.backbone.set_train(mode=mode)


class GridRcnn_Infer(nn.Cell):
    """GridRCNN wrapper for inference."""

    def __init__(self, config):
        super(GridRcnn_Infer, self).__init__()
        self.net = Grid_Rcnn(config)
        self.net.set_train(False)

    def construct(self, img_data, img_metas=None):
        """Make predictions."""
        output = self.net(img_data, img_metas, None, None, None)
        bboxes, labels, mask, _, _ = output
        bboxes_tuple = ()
        for i in range(self.net.test_batch_size):
            bb = self.norm_bboxes(bboxes[i], img_metas[i])
            bboxes_tuple += (bb,)
        bboxes = ms.ops.stack(bboxes_tuple)
        return bboxes, labels, mask

    @staticmethod
    def norm_bboxes(boxes, meta):
        """Norm bboxes according original shape."""
        boxes = boxes.copy()
        boxes[..., [0, 2]] = ms.ops.clip_by_value(
            boxes[..., [0, 2]], clip_value_min=0, clip_value_max=meta[3]
        )
        boxes[..., [1, 3]] = ms.ops.clip_by_value(
            boxes[..., [1, 3]], clip_value_min=0, clip_value_max=meta[2]
        )
        boxes[..., [0, 2]] /= meta[3]
        boxes[..., [1, 3]] /= meta[2]
        boxes[..., [0, 2]] *= meta[1]
        boxes[..., [1, 3]] *= meta[0]
        return boxes
