# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
#
# This file has been derived from the https://github.com/STVIR/Grid-R-CNN
# repository and modified.
# ============================================================================
"""GridHead block for Grid R-CNN"""
import functools

import mindspore as ms
from mindspore import nn


class GridHead(nn.Cell):
    """

    Args:
        cfg (Config): configuration object
        num_convs (int): number of convolutions before grids computation
        roi_feat_size (int): h and w of input feature maps
        in_channels: number of channels of input feature maps
        conv_kernel_size (int): kernel size for convolution layers before grid
            computation
        conv_out_channels (int): number of output channels of convolution
            layer before grid computation
        deconv_kernel (int): deconvolution kernel size (last stage of grid
            computation)
        num_grids (int): number of computed grid points (must be square of
            natural number)
        mask_size (int): size target mask
        loss_weight: weight of grid loss
    """

    def __init__(
            self,
            cfg,
            num_convs=8,
            roi_feat_size=14,
            in_channels=256,
            conv_kernel_size=3,
            conv_out_channels=576,
            deconv_kernel=4,
            num_grids=9,
            mask_size=56,
            loss_weight=15.
    ):
        super().__init__()
        self.num_convs = num_convs
        self.roi_feat_size = roi_feat_size
        self.in_channels = in_channels
        self.conv_kernel_size = conv_kernel_size
        self.conv_out_channels = conv_out_channels
        self.num_grids = num_grids
        self.grid_size = int(self.num_grids ** 0.5)
        assert self.grid_size ** 2 == self.num_grids, \
            'Num grids must be square of integer number.'

        self.mask_size = mask_size
        self.radius = cfg.grid_radius
        self.loss_weight = loss_weight
        self.ms_type = ms.float32

        # compute point-wise sub-regions
        self.sub_regions = self.calc_sub_regions()

        self.convs = self.create_convs()

        planes = self.conv_out_channels
        self.single_plane = self.conv_out_channels // num_grids

        self.updeconv1 = nn.Conv2dTranspose(
            planes, planes, kernel_size=deconv_kernel, stride=2,
            padding=(deconv_kernel - 2) // 2, group=num_grids, pad_mode='pad',
            has_bias=True
        )
        self.sbn1 = nn.GroupNorm(num_grids, planes)
        self.updeconv2 = nn.Conv2dTranspose(
            planes, num_grids, kernel_size=deconv_kernel, stride=2,
            padding=(deconv_kernel - 2) // 2, group=num_grids, pad_mode='pad',
            has_bias=True
        )

        self.neighbor_points = self.create_neighbor_points()

        self.num_edges = functools.reduce(
            lambda x, y: x + y, map(len, self.neighbor_points)
        )
        self.firstOrderConvs, self.secondOrderConvs = self.create_grid_convs()

        self.concat_0 = ms.ops.Concat(axis=0)
        self.concat_1 = ms.ops.Concat(axis=1)

        self.flatten = ms.ops.Flatten()
        self.loss_cls = ms.ops.BCEWithLogitsLoss()

        self.zero_tensor = ms.Tensor(0., dtype=self.ms_type)
        self.mask_range = ms.ops.arange(0, self.mask_size)
        self.grid_range = ms.ops.arange(0, self.num_grids).reshape(1, -1)
        self.reduce_max = ms.ops.ReduceMax()

    def create_neighbor_points(self):
        """Create information about grid points neighbourhood."""
        neighbor_points = []
        for i in range(self.grid_size):
            for j in range(self.grid_size):
                neighbors = []
                if i > 0:  # left: (i - 1, j)
                    neighbors.append((i - 1) * self.grid_size + j)
                if j > 0:  # up: (i, j - 1)
                    neighbors.append(i * self.grid_size + j - 1)
                if j < self.grid_size - 1:  # down: (i, j + 1)
                    neighbors.append(i * self.grid_size + j + 1)
                if i < self.grid_size - 1:  # right: (i + 1, j)
                    neighbors.append((i + 1) * self.grid_size + j)
                neighbor_points.append(tuple(neighbors))
        return tuple(neighbor_points)

    def create_convs(self):
        """Create convolution layers that are applied before grid computation.
        """
        convs = []
        for i in range(self.num_convs):
            in_channels = (
                self.in_channels if i == 0 else self.conv_out_channels)
            strides = 2 if i == 0 else 1
            padding = (self.conv_kernel_size - 1) // 2
            convs.append(
                nn.SequentialCell(
                    nn.Conv2d(
                        in_channels, self.conv_out_channels,
                        self.conv_kernel_size, strides, padding=padding,
                        pad_mode='pad', has_bias=True
                    ),
                    nn.GroupNorm(36, self.conv_out_channels),
                    nn.ReLU()
                )
            )
        return nn.SequentialCell(*convs)

    def create_grid_convs(self):
        """Create convolution layers that applied to compute grids."""
        firstOrderConvs = ms.nn.CellList()
        secondOrderConvs = ms.nn.CellList()
        for _, _point in enumerate(self.neighbor_points):
            _foc = ms.nn.CellList()
            _soc = ms.nn.CellList()
            for _ in range(len(_point)):
                _f = nn.SequentialCell(
                    nn.Conv2d(
                        self.single_plane, self.single_plane, 5, 1, padding=2,
                        pad_mode='pad', group=self.single_plane, has_bias=True
                    ),
                    nn.Conv2d(
                        self.single_plane, self.single_plane, 1, 1, padding=0,
                        has_bias=True
                    )
                )
                _s = nn.SequentialCell(
                    nn.Conv2d(
                        self.single_plane, self.single_plane, 5, 1, padding=2,
                        pad_mode='pad', group=self.single_plane, has_bias=True
                    ),
                    nn.Conv2d(
                        self.single_plane, self.single_plane, 1, 1, padding=0,
                        has_bias=True
                    )
                )
                _foc.append(_f)
                _soc.append(_s)

            firstOrderConvs.append(_foc)
            secondOrderConvs.append(_soc)
        return firstOrderConvs, secondOrderConvs

    def construct(self, x, grid_targets, mask):
        """Compute grids or loss."""
        x = self.convs(x)

        first_order_x = [None] * self.num_grids
        for _idx, _point_idx in enumerate(self.neighbor_points):
            first_order_x[_idx] = (
                x[:, _idx * self.single_plane:(_idx + 1) * self.single_plane]
            )
            for _iidx, _neighbor_idx in enumerate(_point_idx):
                first_order_x[_idx] = (
                    first_order_x[_idx] +
                    self.firstOrderConvs[_idx][_iidx](
                        x[
                            :, _neighbor_idx * self.single_plane:
                            (_neighbor_idx + 1) * self.single_plane
                        ]
                    )
                )

        second_order_x = [None] * self.num_grids
        for _idx, _point_idx in enumerate(self.neighbor_points):
            second_order_x[_idx] = (
                x[:, _idx * self.single_plane:(_idx + 1) * self.single_plane]
            )
            for _iidx, _neighbor_idx in enumerate(_point_idx):
                second_order_x[_idx] = (
                    second_order_x[_idx] +
                    self.secondOrderConvs[_idx][_iidx](
                        first_order_x[_neighbor_idx]
                    )
                )

        x2 = self.concat_1(second_order_x)
        x2 = self.updeconv1(x2)
        x2 = ms.ops.functional.relu(self.sbn1(x2))
        x2 = self.updeconv2(x2)

        if self.training:
            x1 = x
            x1 = self.updeconv1(x1)
            x1 = ms.ops.functional.relu(self.sbn1(x1))
            x1 = self.updeconv2(x1)
            loss = self.loss(x1, x2, grid_targets, mask)

            return loss

        return x2

    def get_target(self, pos_bboxes, pos_gt_bboxes):
        """Compute targets.

           We don't care about image_idx and mix all samples(across images)
           together.
        """
        assert pos_bboxes.shape == pos_gt_bboxes.shape

        # expand pos_bboxes
        x1 = pos_bboxes[:, 0] - (pos_bboxes[:, 2] - pos_bboxes[:, 0]) / 2
        y1 = pos_bboxes[:, 1] - (pos_bboxes[:, 3] - pos_bboxes[:, 1]) / 2
        x2 = pos_bboxes[:, 2] + (pos_bboxes[:, 2] - pos_bboxes[:, 0]) / 2
        y2 = pos_bboxes[:, 3] + (pos_bboxes[:, 3] - pos_bboxes[:, 1]) / 2

        pos_bboxes = self.concat_1([x.view(-1, 1) for x in [x1, y1, x2, y2]])

        R = pos_bboxes.shape[0]
        G = self.num_grids

        xgrid, ygrid = ms.ops.meshgrid(
            (self.mask_range, self.mask_range)
        )
        xgrid = xgrid.reshape(1, 1, self.mask_size, self.mask_size)
        ygrid = ygrid.reshape(1, 1, self.mask_size, self.mask_size)

        grid_size = self.grid_size
        gridpoints_x = (
            (grid_size // 2 - self.grid_range // grid_size / 2.) *
            (pos_gt_bboxes[:, [0]]) +
            (self.grid_range // grid_size / 2.) *
            (pos_gt_bboxes[:, [2]])
        ) / (grid_size // 2)

        gridpoints_y = (
            (grid_size // 2 - self.grid_range % grid_size / 2.) *
            (pos_gt_bboxes[:, [1]]) +
            (self.grid_range % grid_size / 2.) *
            (pos_gt_bboxes[:, [3]])
        ) / (grid_size // 2)

        cxs = (
            (gridpoints_x - pos_bboxes[:, [0]]) /
            (pos_bboxes[:, [2]] - pos_bboxes[:, [0]]) *
            self.mask_size
        ).reshape(R, G, 1, 1)
        cys = (
            (gridpoints_y - pos_bboxes[:, [1]]) /
            (pos_bboxes[:, [3]] - pos_bboxes[:, [1]]) *
            self.mask_size
        ).reshape(R, G, 1, 1)

        cxs = ms.ops.floor(cxs)
        cys = ms.ops.floor(cys)

        targets = (xgrid - cxs) ** 2 + (ygrid - cys) ** 2 <= self.radius

        zero_targets = ms.ops.logical_or(
            (pos_bboxes[:, 2] - pos_bboxes[:, 0]) <= self.grid_size,
            (pos_bboxes[:, 3] - pos_bboxes[:, 1]) <= self.grid_size
        )
        zero_targets = ms.ops.logical_not(zero_targets)
        zero_targets = zero_targets.reshape(-1, 1, 1, 1)

        targets = ms.ops.logical_and(targets, zero_targets)

        targets = self.cast(targets, self.ms_type)
        targets = self.reduce_vision(targets)

        return targets

    def loss(self, grid_pred1, grid_pred2, grid_targets, mask):
        """Compute loss."""
        weights = self.cast(mask, self.ms_type).reshape((-1, 1, 1, 1))
        grid_loss = (
            self.loss_cls(
                grid_pred1, grid_targets, weights,
                ms.ops.ones_like(grid_targets)
            ) +
            self.loss_cls(
                grid_pred2, grid_targets, weights,
                ms.ops.ones_like(grid_targets)
            )
        )
        grid_loss = grid_loss * self.loss_weight
        return grid_loss

    def get_bboxes(self, detected_bboxes, grid_pred, img_meta):
        """Compute bboxes by anchors and grid masks."""
        assert detected_bboxes.shape[0] == grid_pred.shape[0]
        cls_scores = detected_bboxes[:, [4]]
        det_bboxes = detected_bboxes[:, :4]
        grid_pred = ms.ops.sigmoid(grid_pred)

        # expand pos_bboxes
        widths = det_bboxes[:, 2] - det_bboxes[:, 0]
        heights = det_bboxes[:, 3] - det_bboxes[:, 1]
        x1 = det_bboxes[:, 0] - widths / 2
        y1 = det_bboxes[:, 1] - heights / 2

        R, C, H, W = grid_pred.shape
        grid_pred = grid_pred.view(R * C, H * W)
        pred_position, pred_scores = ms.ops.max(grid_pred, axis=1)

        ys = pred_position / W
        xs = pred_position - ys * W

        for i in range(self.num_grids):
            xs[i::self.num_grids] += self.sub_regions[i][0]
            ys[i::self.num_grids] += self.sub_regions[i][1]

        pred_scores, xs, ys = tuple(
            x.view(R, C) for x in [pred_scores, xs, ys]
        )

        grid_points = (
            (
                (xs + 0.5) / (2 * W) * widths.view(-1, 1) * 2 +
                x1.view(-1, 1)
            ),
            (
                (ys + 0.5) / (2 * H) * heights.view(-1, 1) * 2 +
                y1.view(-1, 1)
            )
        )

        x1_inds = list(range(self.grid_size))
        y1_inds = [i * self.grid_size for i in range(self.grid_size)]
        x2_inds = [
            self.num_grids - self.grid_size + i
            for i in range(self.grid_size)
        ]
        y2_inds = [(i + 1) * self.grid_size - 1 for i in range(self.grid_size)]

        res_dets_x1 = (
            (grid_points[0][:, x1_inds] * pred_scores[:, x1_inds]).sum(
                axis=1, keepdims=True
            ) / (pred_scores[:, x1_inds].sum(axis=1, keepdims=True))
        )
        res_dets_y1 = (
            (grid_points[1][:, y1_inds] * pred_scores[:, y1_inds]).sum(
                axis=1, keepdims=True
            ) / (pred_scores[:, y1_inds].sum(axis=1, keepdims=True))
        )
        res_dets_x2 = (
            (grid_points[0][:, x2_inds] * pred_scores[:, x2_inds]).sum(
                axis=1, keepdims=True
            ) / (pred_scores[:, x2_inds].sum(axis=1, keepdims=True))
        )
        res_dets_y2 = (
            (grid_points[1][:, y2_inds] * pred_scores[:, y2_inds]).sum(
                axis=1, keepdims=True
            ) / (pred_scores[:, y2_inds].sum(axis=1, keepdims=True))
        )

        det_res = self.concat_1(
            [res_dets_x1, res_dets_y1, res_dets_x2, res_dets_y2, cls_scores]
        )
        det_res[:, [0, 2]] = ms.ops.clip_by_value(
            det_res[:, [0, 2]], clip_value_min=self.zero_tensor,
            clip_value_max=img_meta[0][3] - 1
        )
        det_res[:, [1, 3]] = ms.ops.clip_by_value(
            det_res[:, [1, 3]], clip_value_min=self.zero_tensor,
            clip_value_max=img_meta[0][2] - 1
        )

        return det_res

    def reduce_vision(self, x):
        """Reduce target to subregion size."""
        layers = []
        for i in range(self.num_grids):
            sub_x1, sub_y1, sub_x2, sub_y2 = self.sub_regions[i]
            layers.append(x[:, [i], sub_y1:sub_y2, sub_x1:sub_x2])
        layers = self.concat_1(layers)
        return layers

    def calc_sub_regions(self):
        """Compute subregions coordinates."""
        half_size = self.mask_size // 4 * 2
        sub_regions = []
        for i in range(self.num_grids):
            x_idx = i // self.grid_size
            y_idx = i % self.grid_size
            if x_idx == 0:
                sub_x1 = 0
            elif x_idx == self.grid_size - 1:
                sub_x1 = half_size
            else:
                ratio = x_idx / (self.grid_size - 1)
                sub_x1 = max(int(ratio * half_size), 0)

            if y_idx == 0:
                sub_y1 = 0
            elif y_idx == self.grid_size - 1:
                sub_y1 = half_size
            else:
                ratio = y_idx / (self.grid_size - 1)
                sub_y1 = max(int(ratio * half_size), 0)
            sub_regions.append(
                (sub_x1, sub_y1, sub_x1 + half_size, sub_y1 + half_size))
        return sub_regions
