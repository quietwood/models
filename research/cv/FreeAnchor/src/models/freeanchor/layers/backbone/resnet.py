# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
#
# This file has been derived from the https://github.com/open-mmlab/mmdetection/tree/v2.28.2
# repository and modified.
# ============================================================================
"""Resnet backbone."""
import numpy as np
import mindspore as ms
from mindspore import nn
from mindspore.common.tensor import Tensor


def weight_init_ones(shape):
    """Weight init."""
    return Tensor(np.full(shape, 0.01).astype(np.float32))


def _conv(in_channels, out_channels, kernel_size=3, stride=1, padding=0, pad_mode='pad'):
    """Conv2D wrapper."""
    shape = (out_channels, in_channels, kernel_size, kernel_size)
    weights = ms.common.initializer.initializer(
        "HeNormal", shape=shape, dtype=ms.float32
    ).init_data()
    return nn.Conv2d(in_channels, out_channels,
                     kernel_size=kernel_size, stride=stride, padding=padding,
                     pad_mode=pad_mode, weight_init=weights, has_bias=False)


def _BatchNorm2dInit(out_chls, momentum=0.1, affine=True, use_batch_statistics=True):
    """Batchnorm2D wrapper."""
    dtype = np.float32
    gamma_init = Tensor(np.array(np.ones(out_chls)).astype(dtype))
    beta_init = Tensor(np.array(np.ones(out_chls) * 0).astype(dtype))
    moving_mean_init = Tensor(np.array(np.ones(out_chls) * 0).astype(dtype))
    moving_var_init = Tensor(np.array(np.ones(out_chls)).astype(dtype))
    return nn.BatchNorm2d(
        out_chls, momentum=momentum, affine=affine, gamma_init=gamma_init,
        beta_init=beta_init, moving_mean_init=moving_mean_init,
        moving_var_init=moving_var_init,
        use_batch_statistics=use_batch_statistics
    )


class BasicBlock(nn.Cell):
    """Basic ResNet block

    Args:
        inplanes (int): number of input channels
        planes (int): number of output channels
        stride (int): stride size in convolution
        dilation (int): dilation size in convolution
        downsample (ms.nn.Sequential): conv+bn block for residual
        norm_eval (bool): if True, BN layer will have only evaluation behaviour
        weights_update (bool): if False, all convolution layer will be freezed.
    """

    expansion = 1

    def __init__(
            self,
            inplanes,
            planes,
            stride=1,
            dilation=1,
            downsample=None,
            norm_eval=False,
            weights_update=False,
    ):
        super(BasicBlock, self).__init__()

        self.weights_update = weights_update
        self.norm_eval = norm_eval
        self.affine = weights_update

        self.bn1 = nn.BatchNorm2d(planes, affine=self.affine)
        self.bn2 = nn.BatchNorm2d(planes, affine=self.affine)

        self.conv1 = nn.Conv2d(
            inplanes,
            planes,
            3,
            stride=stride,
            padding=dilation,
            pad_mode='pad',
            dilation=dilation,
            has_bias=False
        )

        self.conv2 = nn.Conv2d(
            planes, planes, 3, padding=1, pad_mode='pad',
            has_bias=False
        )

        self.relu = nn.ReLU()
        self.downsample = downsample
        self.stride = stride
        self.dilation = dilation

        if not weights_update:
            self.conv1.weight.requires_grad = False
            self.conv2.weight.requires_grad = False
            if self.downsample is not None:
                self.downsample[0].weight.requires_grad = False

        if self.norm_eval:
            self.bn1 = self.bn1.set_train(False)
            self.bn2 = self.bn2.set_train(False)
            if self.downsample is not None:
                self.downsample[1].set_train(False)

    def construct(self, x):
        """run forward"""
        identity = x

        out = self.conv1(x)
        out = self.bn1(out)
        out = self.relu(out)

        out = self.conv2(out)
        out = self.bn2(out)

        if self.downsample is not None:
            identity = self.downsample(x)

        out += identity
        out = self.relu(out)

        return out

    def set_train(self, mode=True):
        """Set training mode"""
        super().set_train(mode=mode)
        self.bn1.set_train(mode and (not self.norm_eval))
        self.bn2.set_train(mode and (not self.norm_eval))
        if self.downsample is not None:
            self.downsample[1].set_train(
                mode and (not self.norm_eval)
            )


class Bottleneck(nn.Cell):
    """Bottleneck block for ResNet."""

    expansion = 4

    def __init__(
            self,
            inplanes,
            planes,
            stride=1,
            dilation=1,
            downsample=None,
            norm_eval=False,
            weights_update=False,
    ):
        super(Bottleneck, self).__init__()
        self.inplanes = inplanes
        self.planes = planes
        self.stride = stride
        self.dilation = dilation
        self.conv1_stride = 1
        self.conv2_stride = stride

        self.weights_update = weights_update
        self.norm_eval = norm_eval
        self.affine = weights_update

        self.bn1 = nn.BatchNorm2d(planes, affine=self.affine)
        self.bn2 = nn.BatchNorm2d(planes, affine=self.affine)
        self.bn3 = nn.BatchNorm2d(
            planes * self.expansion, affine=self.affine
        )

        self.conv1 = nn.Conv2d(
            inplanes,
            planes,
            kernel_size=1,
            stride=self.conv1_stride,
            has_bias=False
        )
        self.conv2 = nn.Conv2d(
            planes,
            planes,
            kernel_size=3,
            stride=self.conv2_stride,
            padding=dilation,
            pad_mode='pad',
            dilation=dilation,
            has_bias=False
        )
        self.conv3 = nn.Conv2d(
            planes,
            planes * self.expansion,
            kernel_size=1,
            has_bias=False
        )

        self.relu = nn.ReLU()
        self.downsample = downsample

        if not weights_update:
            self.conv1.weight.requires_grad = False
            self.conv2.weight.requires_grad = False
            self.conv3.weight.requires_grad = False
            if self.downsample is not None:
                self.downsample[0].weight.requires_grad = False

        if self.norm_eval:
            self.bn1 = self.bn1.set_train(False)
            self.bn2 = self.bn2.set_train(False)
            self.bn3 = self.bn3.set_train(False)
            if self.downsample is not None:
                self.downsample[1].set_train(False)

    def construct(self, x):
        """run forward"""
        identity = x

        out = self.conv1(x)
        out = self.bn1(out)
        out = self.relu(out)

        out = self.conv2(out)
        out = self.bn2(out)
        out = self.relu(out)

        out = self.conv3(out)
        out = self.bn3(out)

        if self.downsample is not None:
            identity = self.downsample(x)
        out += identity
        out = self.relu(out)

        return out

    def set_train(self, mode=True):
        """set train mode."""
        super(Bottleneck, self).set_train(mode=mode)
        self.bn1.set_train(mode and (not self.norm_eval))
        self.bn2.set_train(mode and (not self.norm_eval))
        self.bn3.set_train(mode and (not self.norm_eval))
        if self.downsample is not None:
            self.downsample[1].set_train(
                mode and (not self.norm_eval)
            )


class ResNet(nn.Cell):
    """ResNet backbone.

    Args:
        depth (int): Depth of resnet, from {18, 34, 50, 101, 152}.
        num_stages (int): Resnet stages, normally 4.
        strides (Sequence[int]): Strides of the first block of each stage.
        dilations (Sequence[int]): Dilation of each stage.
        out_indices (Sequence[int]): Output from which stages.
        frozen_stages (int): Stages to be frozen (stop grad and set eval mode).
            `-1` means not freezing any parameters.
        norm_eval (bool): Whether to set norm layers to eval mode, namely,
            freeze running stats (mean and var). Note: Effect on Batch Norm
            and its variants only.
    """

    arch_settings = {
        18: (BasicBlock, (2, 2, 2, 2)),
        34: (BasicBlock, (3, 4, 6, 3)),
        50: (Bottleneck, (3, 4, 6, 3)),
        101: (Bottleneck, (3, 4, 23, 3)),
        152: (Bottleneck, (3, 8, 36, 3))
    }

    def __init__(self,
                 depth,
                 num_stages=4,
                 strides=(1, 2, 2, 2),
                 dilations=(1, 1, 1, 1),
                 out_indices=(0, 1, 2, 3),
                 frozen_stages=-1,
                 norm_eval=False
                 ):
        super(ResNet, self).__init__()
        self.depth = depth
        assert num_stages >= 1
        assert num_stages <= 4
        self.num_stages = num_stages
        assert len(strides) == len(dilations)
        assert len(dilations) == num_stages
        self.strides = strides
        self.dilations = dilations
        assert max(out_indices) < num_stages
        self.out_indices = out_indices
        self.frozen_stages = frozen_stages
        self.norm_eval = norm_eval
        self.block, stage_blocks = self.arch_settings[self.depth]
        self.stage_blocks = stage_blocks[:self.num_stages]
        self.inplanes = 64

        self._make_stem_layer()

        self.inplanes, (
            self.layer1, self.layer2, self.layer3, self.layer4
        ) = self.get_res_blocks(
            self.stage_blocks, self.strides, self.dilations,
            self.block, self.inplanes, self.frozen_stages, norm_eval
        )

        self.feat_dim = self.block.expansion * 64 * 2**(
            len(self.stage_blocks) - 1)

    @staticmethod
    def get_res_blocks(stage_blocks, strides, dilations, block, inplanes,
                       frozen_stages, norm_eval):
        res_layers = []
        for i, num_blocks in enumerate(stage_blocks):
            stride = strides[i]
            dilation = dilations[i]
            planes = 64 * 2**i
            res_layer = make_res_layer(
                block,
                inplanes,
                planes,
                num_blocks,
                stride=stride,
                dilation=dilation,
                weights_update=(i > frozen_stages),
                norm_eval=norm_eval
            )
            inplanes = planes * block.expansion
            res_layers.append(res_layer)
        for i in range(4 - len(stage_blocks)):
            res_layers.append(nn.Identity())
        return inplanes, res_layers

    def _make_stem_layer(self):
        weights_update = self.frozen_stages >= 0
        self.conv1 = nn.Conv2d(
            3,
            64,
            kernel_size=7,
            stride=2,
            padding=3,
            pad_mode='pad',
            has_bias=False)
        self.bn1 = nn.BatchNorm2d(64, affine=weights_update)

        if weights_update:
            self.conv1.weight.requires_grad = False
        if self.norm_eval:
            self.bn1.set_train(False)

        self.relu = nn.ReLU()

        self.maxpool = nn.SequentialCell(
            nn.Pad(((0, 0), (0, 0), (1, 1), (1, 1))),
            nn.MaxPool2d(
                kernel_size=3, stride=2, pad_mode='valid'
            )
        )

    def construct(self, x):
        x = self.conv1(x)
        x = self.bn1(x)
        x = self.relu(x)
        x = self.maxpool(x)
        outs = []
        x = self.layer1(x)
        if 0 in self.out_indices:
            outs.append(x)
        x = self.layer2(x)
        if 1 in self.out_indices:
            outs.append(x)
        x = self.layer3(x)
        if 2 in self.out_indices:
            outs.append(x)
        x = self.layer4(x)
        if 3 in self.out_indices:
            outs.append(x)
        return tuple(outs)

    def set_train(self, mode=True):
        super(ResNet, self).set_train(mode=mode)
        self.bn1.set_train(mode and (not self.norm_eval))
        for block in self.layer1:
            block.set_train(mode)
        for block in self.layer2:
            block.set_train(mode)
        for block in self.layer3:
            block.set_train(mode)
        for block in self.layer4:
            block.set_train(mode)


def make_res_layer(
        block,
        inplanes,
        planes,
        blocks,
        stride=1,
        dilation=1,
        weights_update=True,
        norm_eval=False
):
    """Make residual block."""
    downsample = None
    if stride != 1 or inplanes != planes * block.expansion:
        downsample = nn.SequentialCell(
            nn.Conv2d(
                inplanes,
                planes * block.expansion,
                kernel_size=1,
                stride=stride,
                has_bias=False),
            nn.BatchNorm2d(planes * block.expansion),
        )

    layers = [
        block(
            inplanes,
            planes,
            stride,
            dilation,
            downsample,
            norm_eval=norm_eval,
            weights_update=weights_update
        )
    ]
    inplanes = planes * block.expansion
    for _ in range(1, blocks):
        layers.append(
            block(
                inplanes,
                planes,
                1,
                dilation,
                norm_eval=norm_eval,
                weights_update=weights_update
            )
        )

    return nn.SequentialCell(*layers)
