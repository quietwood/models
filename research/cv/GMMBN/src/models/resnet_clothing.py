import math
import mindspore
import mindspore.nn as nn
import mindspore.ops as ops
from mindspore import Parameter
from .ltbn import TwoInputSequential, AugBatchNorm, TwoConv2d

def conv3x3(in_planes, out_planes, stride=1):
    return nn.Conv2d(in_planes, out_planes, kernel_size=3, stride=stride, pad_mode='pad', padding=1)

class NormedLinear(nn.Cell):
    def __init__(self, in_features, out_features):
        super(NormedLinear, self).__init__()
        self.weight = Parameter(mindspore.Tensor(in_features, out_features))
        self.weight.data.uniform_(-1, 1).renorm_(2, 1, 1e-5).mul_(1e5)

    def construct(self, x):
        out = ops.MatMul()(ops.L2Normalize(axis=1)(x), ops.L2Normalize(axis=0)(self.weight))
        return out

def NormedLayer(channel=32, momentum=0.5):
    return AugBatchNorm(num_features=channel, momentum=momentum)#nn.BatchNorm2d(num_features=channel,momentum=momentum)

class BasicBlock(nn.Cell):
    expansion = 1
    def __init__(self, inplanes, planes, stride=1, downsample=None, bnm=0.9):
        super(BasicBlock, self).__init__()
        self.conv1 = conv3x3(inplanes, planes, stride)
        self.bn1 = NormedLayer(planes, momentum=bnm)
        self.relu = nn.ReLU()
        self.conv2 = conv3x3(planes, planes)
        self.bn2 = NormedLayer(planes, momentum=bnm)
        self.downsample = downsample
        self.stride = stride

    def construct(self, x, idx=0):
        residual = x

        out = self.conv1(x)
        out, _ = self.bn1(out, idx)
        out = self.relu(out)
        out = self.conv2(out)
        out, _ = self.bn2(out, idx)

        if self.downsample is not None:
            residual, _ = self.downsample(x, idx)

        out += residual
        out = self.relu(out)

        return out, idx

class Bottleneck(nn.Cell):
    expansion = 4

    def __init__(self, inplanes, planes, stride=1, downsample=None, bnm=0.9):
        super(Bottleneck, self).__init__()
        self.conv1 = nn.Conv2d(inplanes, planes, kernel_size=1)
        self.bn1 = NormedLayer(planes, momentum=bnm)
        self.conv2 = nn.Conv2d(planes, planes, kernel_size=3, stride=stride, pad_mode='pad', padding=1)
        self.bn2 = NormedLayer(planes, momentum=bnm)
        self.conv3 = nn.Conv2d(planes, planes * 4, kernel_size=1)
        self.bn3 = NormedLayer(planes * 4, momentum=bnm)
        self.relu = nn.ReLU()
        self.downsample = downsample
        self.stride = stride

    def construct(self, x, idx=0):
        residual = x

        out = self.conv1(x)
        out, _ = self.bn1(out, idx)
        out = self.relu(out)
        out = self.conv2(out)
        out, _ = self.bn2(out, idx)
        out = self.relu(out)
        out = self.conv3(out)
        out, _ = self.bn3(out, idx)

        if self.downsample is not None:
            residual, _ = self.downsample(x, idx)

        out += residual
        out = self.relu(out)

        return out, idx

class ResNet(nn.Cell):
    def __init__(self, block, layers, num_classes=1000, use_norm=False, bnm=0.5, channel=3, sync=True, k=4):
        self.inplanes = 64
        self.num_classes = num_classes
        super(ResNet, self).__init__()
        self.bnm = bnm

        self.conv1 = nn.Conv2d(channel, 64, kernel_size=7, stride=2, pad_mode='pad', padding=3)
        self.bn1 = NormedLayer(channel=64, momentum=self.bnm)
        self.relu = nn.ReLU()
        self.maxpool = nn.MaxPool2d(kernel_size=3, stride=2, pad_mode='pad', padding=1)
        self.layer1 = self._make_layer(block, 64, layers[0])
        self.layer2 = self._make_layer(block, 128, layers[1], stride=2)
        self.layer3 = self._make_layer(block, 256, layers[2], stride=2)
        self.layer4 = self._make_layer(block, 512, layers[3], stride=2)
        self.avgpool = nn.AdaptiveAvgPool2d((1, 1))
        if use_norm:
            self.fc = NormedLinear(512 * block.expansion, num_classes)
        else:
            self.fc = nn.Linear(512 * block.expansion, num_classes)

        for m in self.modules():
            if isinstance(m, (nn.Conv2d, TwoConv2d)):
                n = m.kernel_size[0] * m.kernel_size[1] * m.out_channels
                m.weight.data.normal_(0, math.sqrt(2. / n))
            elif isinstance(m, nn.Linear):
                m.weight.data.normal_(0, 0.01)
                m.bias.data.zero_()
            elif isinstance(m, nn.BatchNorm2d):
                m.weight.data.fill_(1)
                m.bias.data.zero_()

    def _make_layer(self, block, planes, blocks, stride=1):
        downsample = None
        if stride != 1 or self.inplanes != planes * block.expansion:
            downsample = TwoInputSequential(
                TwoConv2d(self.inplanes, planes * block.expansion, kernel_size=1, stride=stride, bias=False),
                NormedLayer(planes * block.expansion, momentum=self.bnm),
            )

        layers = []
        layers.append(block(self.inplanes, planes, stride, downsample, self.bnm))
        self.inplanes = planes * block.expansion
        for _ in range(1, blocks):
            layers.append(block(self.inplanes, planes, bnm=self.bnm))

        return TwoInputSequential(*layers)

    def forward(self, x, idx=0):
        x = self.conv1(x)
        x, _ = self.bn1(x, idx)
        x = self.relu(x)
        x = self.maxpool(x)
        x, _ = self.layer1(x, idx)
        x, _ = self.layer2(x, idx)
        x, _ = self.layer3(x, idx)
        x, _ = self.layer4(x, idx)
        x = self.avgpool(x)
        x = ops.flatten(x)
        x = self.fc(x)

        return x
