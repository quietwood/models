import random
from PIL import Image, ImageEnhance, ImageOps


class ShearX:
    def __init__(self, fillcolor=(128, 128, 128)):
        self.fillcolor = fillcolor

    def __call__(self, x, magnitude):
        return Image.fromarray(x).transform(
            Image.fromarray(x).size, Image.AFFINE, (1, magnitude * random.choice([-1, 1]), 0, 0, 1, 0),
            Image.BICUBIC, fillcolor=self.fillcolor)


class ShearY:
    def __init__(self, fillcolor=(128, 128, 128)):
        self.fillcolor = fillcolor

    def __call__(self, x, magnitude):
        return Image.fromarray(x).transform(
            Image.fromarray(x).size, Image.AFFINE, (1, 0, 0, magnitude * random.choice([-1, 1]), 1, 0),
            Image.BICUBIC, fillcolor=self.fillcolor)


class TranslateX:
    def __init__(self, fillcolor=(128, 128, 128)):
        self.fillcolor = fillcolor

    def __call__(self, x, magnitude):
        return Image.fromarray(x).transform(
            Image.fromarray(x).size, Image.AFFINE, \
                (1, 0, magnitude * Image.fromarray(x).size[0] * random.choice([-1, 1]), 0, 1, 0),
            fillcolor=self.fillcolor)


class TranslateY:
    def __init__(self, fillcolor=(128, 128, 128)):
        self.fillcolor = fillcolor

    def __call__(self, x, magnitude):
        return Image.fromarray(x).transform(
            Image.fromarray(x).size, Image.AFFINE, \
                (1, 0, 0, 0, 1, magnitude * Image.fromarray(x).size[1] * random.choice([-1, 1])),
            fillcolor=self.fillcolor)


class Rotate:
    # from https://stackoverflow.com/questions/
    # 5252170/specify-image-filling-color-when-rotating-in-python-with-pil-and-setting-expand
    def __call__(self, x, magnitude):
        rot = Image.fromarray(x).convert("RGBA").rotate(magnitude * random.choice([-1, 1]))
        return Image.composite(rot, Image.new("RGBA", rot.size, (128,) * 4), rot).convert(Image.fromarray(x).mode)


class Color:
    def __call__(self, x, magnitude):
        return ImageEnhance.Color(Image.fromarray(x)).enhance(1 + magnitude * random.choice([-1, 1]))


class Posterize:
    def __call__(self, x, magnitude):
        return ImageOps.posterize(Image.fromarray(x), magnitude)


class Solarize:
    def __call__(self, x, magnitude):
        return ImageOps.solarize(Image.fromarray(x), magnitude)


class Contrast:
    def __call__(self, x, magnitude):
        return ImageEnhance.Contrast(Image.fromarray(x)).enhance(1 + magnitude * random.choice([-1, 1]))


class Sharpness:
    def __call__(self, x, magnitude):
        return ImageEnhance.Sharpness(Image.fromarray(x)).enhance(1 + magnitude * random.choice([-1, 1]))


class Brightness:
    def __call__(self, x, magnitude):
        return ImageEnhance.Brightness(Image.fromarray(x)).enhance(1 + magnitude * random.choice([-1, 1]))


class AutoContrast:
    def __call__(self, x, magnitude):
        return ImageOps.autocontrast(Image.fromarray(x))


class Equalize:
    def __call__(self, x, magnitude):
        return ImageOps.equalize(Image.fromarray(x))


class Invert:
    def __call__(self, x, magnitude):
        return ImageOps.invert(Image.fromarray(x))
