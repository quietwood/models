import sys
import re
import fnmatch
from collections import defaultdict
from copy import deepcopy

__all__ = ['list_dataset', 'is_dataset', 'dataset_entry', 'list_modules', 'is_data_in_modules',
        'is_dataset_default_key', 'has_dataset_default_key', 'get_dataset_default_value']

_dataset_entry = dict()
_module_to_dataset = defaultdict(set)
_dataset_to_module = dict()
_dataset_default_cfgs = dict()

def register_dataset(fn):

    mod = sys.modules[fn.__module__]
    module_name_split = fn.__module__.split('.')
    module_name = module_name_split[-1] if module_name_split else ''

    #add dataset to __all__ in module
    dataset_name = fn.__name__
    if hasattr(mod, '__all__'):
        mod.__all__.append(dataset_name)
    else:
        mod.__all__ = [dataset_name]

    #add entries to registry dict
    _dataset_entry[dataset_name] = fn
    _dataset_to_module[dataset_name] = module_name
    _module_to_dataset[module_name].add(dataset_name)

    #
    if hasattr(mod, 'default_cfgs') and dataset_name in mod.default_cfgs:
        _dataset_default_cfgs[dataset_name] = deepcopy(mod.default_cfgs[dataset_name])

    return fn

def _natural_key(string_):
    return [int(s) if s.isdigit() else s for s in re.split(r'(\d+)', string_.lower())]

def list_dataset(filters='', module='', exclude_filters='', name_matches_cfg=False):
    if module:
        all_dataset = list(_module_to_dataset[module])
    else:
        all_dataset = _dataset_entry.keys()

    if filters:
        datasets = []
        include_filters = filters if isinstance(filters, (tuple, list)) else [filters]
        for f in include_filters:
            include_datasets = fnmatch.filter(all_dataset, f)  # include these models
            if include_datasets:
                datasets = set(datasets).union(include_datasets)
    else:
        datasets = all_dataset

    if exclude_filters:
        if not isinstance(exclude_filters, (tuple, list)):
            exclude_filters = [exclude_filters]
        for xf in exclude_filters:
            exclude_datasets = fnmatch.filter(datasets, xf)  # exclude these models
            if exclude_datasets:
                datasets = set(datasets).difference(exclude_datasets)
    if name_matches_cfg:
        datasets = set(_dataset_default_cfgs).intersection(datasets)
    return list(sorted(datasets, key=_natural_key))

def is_dataset(dataset_name):
    return dataset_name in _dataset_entry

def dataset_entry(dataset_name):
    return _dataset_entry[dataset_name]

def list_modules():
    modules = _module_to_dataset.keys()
    return list(sorted(modules))


def is_data_in_modules(dataset_name, module_names):
    assert isinstance(module_names, (tuple, list, set))
    return any(dataset_name in _module_to_dataset[n] for n in module_names)


def has_dataset_default_key(dataset_name, cfg_key):
    """ Query dataset default_cfgs for existence of a specific key.
    """
    if dataset_name in _dataset_default_cfgs and cfg_key in _dataset_default_cfgs[dataset_name]:
        return True
    return False


def is_dataset_default_key(dataset_name, cfg_key):
    """ Return truthy value for specified dataset default_cfg key, False if does not exist.
    """
    if dataset_name in _dataset_default_cfgs and _dataset_default_cfgs[dataset_name].get(cfg_key, False):
        return True
    return False


def get_dataset_default_value(dataset_name, cfg_key):
    """ Get a specific dataset default_cfg value by key. None if it doesn't exist.
    """
    if dataset_name in _dataset_default_cfgs:
        return _dataset_default_cfgs[dataset_name].get(cfg_key, None)
    return None
