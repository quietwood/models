## basic package
import sys
import re

import fnmatch
from copy import deepcopy
from collections import defaultdict
import numpy as np

## pytorch package
import mindspore
import mindspore.ops as ops
from mindspore import Tensor



__all__ = ['list_sampler', 'is_sampler', 'sampler_entry', 'list_modules', 'is_sampler_in_modules',
        'is_sampler_default_key', 'has_sampler_default_key', 'get_sampler_default_value']

_sampler_entry = dict()
_module_to_sampler = defaultdict(set)
_sampler_to_module = dict()
_sampler_default_cfgs = dict()

def register_sampler(fn):

    mod = sys.modules[fn.__module__]
    module_name_split = fn.__module__.split('.')
    module_name = module_name_split[-1] if module_name_split else ''
    print(module_name_split, module_name)

    #add sampler to __all__ in module
    sampler_name = fn.__name__
    if hasattr(mod, '__all__'):
        mod.__all__.append(sampler_name)
    else:
        mod.__all__ = [sampler_name]

    #add entries to registry dict
    _sampler_entry[sampler_name] = fn
    _sampler_to_module[sampler_name] = module_name
    _module_to_sampler[module_name].add(sampler_name)

    #
    if hasattr(mod, 'default_cfgs') and sampler_name in mod.default_cfgs:
        _sampler_default_cfgs[sampler_name] = deepcopy(mod.default_cfgs[sampler_name])

    return fn

def _natural_key(string_):
    return [int(s) if s.isdigit() else s for s in re.split(r'(\d+)', string_.lower())]

def list_sampler(filters='', module='', exclude_filters='', name_matches_cfg=False):
    if module:
        all_sampler = list(_module_to_sampler[module])
    else:
        all_sampler = _sampler_entry.keys()

    if filters:
        sampler = []
        include_filters = filters if isinstance(filters, (tuple, list)) else [filters]
        for f in include_filters:
            include_sampler = fnmatch.filter(all_sampler, f)  # include these models
            if include_sampler:
                sampler = set(sampler).union(include_sampler)
    else:
        sampler = all_sampler

    if exclude_filters:
        if not isinstance(exclude_filters, (tuple, list)):
            exclude_filters = [exclude_filters]
        for xf in exclude_filters:
            exclude_sampler = fnmatch.filter(sampler, xf)  # exclude these models
            if exclude_sampler:
                sampler = set(sampler).difference(exclude_sampler)
    if name_matches_cfg:
        sampler = set(_sampler_default_cfgs).intersection(sampler)
    return list(sorted(sampler, key=_natural_key))

def is_sampler(sampler_name):
    return sampler_name in _sampler_entry

def sampler_entry(sampler_name):
    return _sampler_entry[sampler_name]

def list_modules():
    modules = _module_to_sampler.keys()
    return list(sorted(modules))


def is_sampler_in_modules(sampler_name, module_names):
    assert isinstance(module_names, (tuple, list, set))
    return any(sampler_name in _module_to_sampler[n] for n in module_names)


def has_sampler_default_key(sampler_name, cfg_key):
    """ Query sampler default_cfgs for existence of a specific key.
    """
    if sampler_name in _sampler_default_cfgs and cfg_key in _sampler_default_cfgs[sampler_name]:
        return True
    return False


def is_sampler_default_key(sampler_name, cfg_key):
    """ Return truthy value for specified sampler default_cfg key, False if does not exist.
    """
    if sampler_name in _sampler_default_cfgs and _sampler_default_cfgs[sampler_name].get(cfg_key, False):
        return True
    return False


def get_sampler_default_value(sampler_name, cfg_key):
    """ Get a specific sampler default_cfg value by key. None if it doesn't exist.
    """
    if sampler_name in _sampler_default_cfgs:
        return _sampler_default_cfgs[sampler_name].get(cfg_key, None)
    return None

########################################################################################################################
class ImbalancedDatasetSampler(mindspore.dataset.Sampler):

    def __init__(self, dataset, indices=None, num_samples=None):
        # if indices is not provided,
        # all elements in the dataset will be considered
        self.indices = list(range(len(dataset))) \
            if indices is None else indices

        # if num_samples is not provided,
        # draw `len(indices)` samples in each iteration
        self.num_samples = len(self.indices) \
            if num_samples is None else num_samples

        # distribution of classes in the dataset
        label_to_count = [0] * len(np.unique(dataset.targets))
        for idx in self.indices:
            label = self._get_label(dataset, idx)
            label_to_count[label] += 1

        beta = 0.9999
        effective_num = 1.0 - np.power(beta, label_to_count)
        per_cls_weights = (1.0 - beta) / np.array(effective_num)

        # weight for each sample
        weights = [per_cls_weights[self._get_label(dataset, idx)]
                   for idx in self.indices]
        self.weights = Tensor(weights)

    def _get_label(self, dataset, idx):
        return dataset.targets[idx]

    def __iter__(self):
        return iter(ops.multinomial(self.weights, self.num_samples, replacement=True).tolist())

    def __len__(self):
        return self.num_samples


@register_sampler
def standard_sampler(general_dataset):
    return ImbalancedDatasetSampler(general_dataset)
