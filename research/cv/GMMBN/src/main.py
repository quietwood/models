import os
import random
import time
from scipy import spatial

import mindspore
from mindspore import Tensor
import mindspore.nn as nn
import mindspore.dataset as ds
from mindspore.communication import init
from mindvision.engine.callback import LossMonitor

import models
# from tensorboardX import SummaryWriter
from utils.utils import get_args
from dataset.dataloader import data_loader
from loss import get_loss

best_acc1 = 0
def cosine_similarity(v1, v2):
    # 输入两个尺寸一样的array
    # 按行求两个矩阵间的余弦相似度 dim=1
    shape = v1.shape
    result = []
    for i in range(shape[0]):
        result.append(1 - spatial.distance.cosine(v1[i], v2[i]))
    return Tensor(result)

def main():
    args = get_args()
    random.seed(args.data_seed)
    mindspore.set_seed(args.data_seed)
    main_worker(args)

def main_worker(args):
    global best_acc1

    # create model
    print("=> creating model '{}'".format(args.arch))
    model = models.create_model(args)
    args.ema_flag = False
    if args.ema_flag is True:
        args.ema = models.ModelEmaV2(model, args.ema_decay)

    base_lr = 0.1*args.batch_size/256 if args.lsr else args.base_lr##Linear Learning rate scaling

    params = model.get_parameters()#models.split_weights(model)
    optimizer = nn.SGD(params, base_lr, momentum=args.momentum, weight_decay=args.weight_decay)

    # Data loading code
    start = time.time()
    args, train_dataset_weak, _, val_dataset = data_loader(args)
    dataset_size = train_dataset_weak.get_dataset_size()
    print("The dataset size is:", train_dataset_weak.get_dataset_size())
    end = time.time()
    print("data_loader time:", end-start)

    criterion = get_loss(args)
    # criterion = nn.SoftmaxCrossEntropyWithLogits(sparse=True, reduction="mean")

    model_path = "./ckpt/"
    os.system('rm -f {0}*.ckpt {0}*.meta {0}*.pb'.format(model_path))
    ckpt_path = " "
    if os.path.exists(ckpt_path):
        print("=> loading checkpoint '{}'".format(ckpt_path))
        checkpoint = mindspore.load_checkpoint(ckpt_path)
        mindspore.load_param_into_net(model, checkpoint)
        print("=> loaded checkpoint '{}'".format(ckpt_path))
    config = mindspore.CheckpointConfig(save_checkpoint_steps=dataset_size, keep_checkpoint_max=35)
    ckpoint = mindspore.ModelCheckpoint(prefix="train_resnet_cifar10", directory=model_path, config=config)
    model = mindspore.Model(network=model, loss_fn=criterion, optimizer=optimizer, metrics={"acc"})
    model.train(100, train_dataset=train_dataset_weak, callbacks=[ckpoint, LossMonitor(0.005)], dataset_sink_mode=False)
    acc = model.eval(val_dataset)
    print("Device:", device_id, "------>", "acc:", acc)

if __name__ == '__main__':
    device_id = int(os.getenv('DEVICE_ID'))
    mindspore.set_context(mode=mindspore.PYNATIVE_MODE, device_target='Ascend', device_id=device_id)
    init()
    ds.config.set_seed(1000) # set dataset seed to make sure than all cards read the same data
    mindspore.set_auto_parallel_context(parallel_mode=mindspore.ParallelMode.DATA_PARALLEL, gradients_mean=True)
    main()
