# 目录

[View English](./README.md)

<!-- TOC -->

- [目录](#目录)
- [GMMBNN描述](#gmmbnn描述)
- [模型架构](#模型架构)
- [数据集](#数据集)
- [环境要求](#环境要求)
- [快速入门](#快速入门)
- [脚本说明](#脚本说明)
    - [脚本及样例代码](#脚本及样例代码)
    - [脚本参数](#脚本参数)
    - [训练过程](#训练过程)
        - [训练](#训练)
        - [分布式训练](#分布式训练)
    - [评估过程](#评估过程)
        - [评估](#评估)
    - [导出过程](#导出过程)
        - [导出](#导出)
- [模型描述](#模型描述)
    - [性能](#性能)
        - [评估性能](#评估性能)
- [ModelZoo主页](#modelzoo主页)

<!-- /TOC -->

# GMMBNN描述

利用数据重采样、重加权和余量调整等鲁棒训练算法，在长尾数据分布下的图像分类神经网络学习方面取得了重大进展。然而，这些方法忽略了数据不平衡对特征归一化的影响。多数类别(头部类别)在估计统计数据和仿射参数方面的优势导致较少频率类别的内部协变量移位被忽视。为了缓解这一挑战，GMMBN提出了一种基于高斯混合的复合批归一化方法。它可以更全面地建模特征空间，减少头部类的主导地位。此外，采用基于移动平均的期望最大化算法对多个高斯分布的统计参数进行估计。然而，EM算法对初始化很敏感，并且很容易陷入局部最小值，其中多个高斯分量继续集中在多数类上。为了解决这个问题，GMMBN开发了一个双路径学习框架，它采用类感知的分割特征归一化来多样化估计的高斯分布，允许高斯分量更全面地适应频率较低的类的训练样本。在常用数据集上的大量实验表明，该方法优于现有的长尾图像分类方法

[论文](https://arxiv.org/abs/2212.01007)：Cheng, Lechao, et al. "Compound Batch Normalization for Long-tailed Image Classification." Proceedings of the 30th ACM International Conference on Multimedia. 2022.

# 模型架构

基于ResNet建立双路径学习模型，在网络前向过程中使用复合归一化，拆分归一化用于归一化底部分支中的中间特征。

# 数据集

使用的数据集：CIFAR10-LT是通过对原始CIFAR-10数据集进行重新采样而形成的。类样本大小服从指数分布。

# 环境要求

- 硬件（Ascend/GPU/CPU）
    - 使用Ascend/GPU/CPU处理器来搭建硬件环境。
- 框架
    mindspore=1.8.1

# 快速入门

通过官方网站安装MindSpore后，您可以按照如下步骤进行训练和评估：

- 在ModelArts上运行
  如果您想在modelart中运行，可以按照如下方式开始训练：

  ```yaml
  # 修改./src/configs/cifar10_im100.yaml配置文件
  data_folder: '/home/ma-user/work/workspace/GMMBN/data/cifar-10-batches-bin'
  ```

    - 1p训练如何启动

    ```shell
    bash ./scripts/main.sh [DATA_CONFIG]
    ```

    - 8p训练如何启动

    ```shell
    bash ./scripts/main8.sh [DATA_CONFIG]
    ```

# 脚本说明

## 脚本及样例代码

```bash
├── GMMBN
    ├── README.md                         // 所有模型相关说明
    ├── scripts                               // 脚本文件
        ├── main.sh                           // 单卡训练脚本
        ├── main8.sh                          // 八卡训练脚本
    ├── src
        ├── eval.py                       // 验证模型
        ├── export.py                     // 实现310推理源代码
        ├── loss.py                       // 损失函数
        ├── main.py                       // 使用Model API训练的主函数
        ├── main_.py                      // 未使用Model API训练的主函数
        ├── configs                       // 配置文件
        │   ├──cifar10_1m100.yaml
        ├── dataset                       // 数据载入和增强处理
        │   ├──auto_aug.py
        │   ├──auto_augment.py
        │   ├──auto_ops.py
        │   ├──balance_sampler.py
        │   ├──dataloader.py
        │   ├──data_registry.py
        │   ├──synthetic.py
        │   ├──transforms.py
        ├── models                         // 模型定义
        │   ├──ema.py
        │   ├──gmm_resnet.py
        │   ├──lookahead.py
        │   ├──ltbn.py
        │   ├──model.py
        │   ├──model_registry.py
        │   ├──resnet_clothing.py
        │   ├──resnet_synthetic.py
        │   ├──__init__.py
        ├── utils                          // 模型定义
        │   ├──loss_registry.py
        │   ├──utils.py
        │   ├──__init__.py

```

## 脚本参数

在cifar10_im100.yml中可以同时配置相关参数。
注：所有参数设置均为单卡

  ```yaml
  ## 数据集设置
  data_folder: '/cifar-10-batches-bin'
  log_folder: './log/'
  checkpoint_folder: './checkpoint/'
  excel: './log/excel/results_cifar10_im100.xlsx'

  data_name: 'cifar10'
  data_ratio: 0.01
  data_seed: 1024
  download: True
  num_class: 10
  input_size: [32,32]
  input_ch: 3

  ## 模型设置
  arch: 'resnet32'
  bn_momentum: 0.1
  pretrained: False
  use_norm: False
  resume: False
  resume_checkpoint: ''
  ema_flag: True
  ema_decay: 0.9996

  ## 优化器设置
  batch_size: 512
  epochs: 100
  workers: 8
  drop_last: False
  pin_memory: True

  amp: False
  apex_amp: False
  native_amp: False
  tta: 0
  local_rank: 0
  split_bn: False
  clip_grad: False
  clip_value: 0.5
  optim_type: 'SGD'
  base_lr: 0.1
  lsr: True #linear scaling rate
  warm_up: 50
  weight_decay: 0.0002
  momentum: 0.9
  start_epoch: 0
  epoch_interval: 10
  print_freq: 10
  loss_type: 'CrossEntropy'

  data_sampler: 'standard_sampler'
  freze_ratio: 0.8
  decoupling: False
  re_weighting: False
  re_sampling: False
  ```

更多配置细节请参考脚本`cifar10_im100.yml`。

## 训练过程

### 训练

```yaml
[DATA_CONFIG]：配置文件路径
[NUM_OF_RANK]: 指定使用NPU数目
```

- Ascend处理器环境运行

  ```bash
  bash ./scripts/main.sh [DATA_CONFIG]
  ```

### 分布式训练

- Ascend处理器环境运行

  ```bash
  bash ./scripts/main8.sh [DATA_CONFIG]
  ```

## 评估过程

### 评估

```yaml
[CKPT_PATH]：待验证文件路径
```

- 在Ascend环境运行时评估CIFAR-10数据集

  ```bash
  bash ./scripts/run_eval.sh [CKPT_PATH]
  ```

## 导出过程

### 导出

```yaml
[EXPORT_MODEL]：要导出为air文件的模型路径
```

```shell
python export.py --export_model [EXPORT_MODEL]
```

# 模型描述

## 性能

### 评估性能

| 参数                 | Ascend                                                      | GPU                    |
| -------------------------- | ----------------------------------------------------------- | ---------------------- |
| 模型版本              | GMMBN                                                | GMMBN           |
| 资源                  | Ascend 910；CPU 2.60GHz，192核；内存 755G；系统 Euler2.8             | NV SMX2 V100-32G       |
| 上传日期              | 2023-09-17                                 | 2023-09-17 |
| MindSpore版本          | 1.8.1                                                       | 1.8.1                  |
| 数据集                    | CIFAR-10                                                    | CIFAR-10               |
| 训练参数        | epoch=100, batch_size = 512, lr=0.1              | epoch=100, batch_size=512, lr=0.1    |
| 优化器                  | SGD                                                   | SGD               |
| 损失函数              | Softmax交叉熵                                       | Softmax交叉熵  |
| 输出                    | 概率                                                 | 概率            |
| 损失                       | 0.074                                                      | 0.074                 |
| 精度                       | 85.6%                                                       | 85.0%                |

# ModelZoo主页  

 请浏览官网[主页](https://gitee.com/mindspore/models)。
