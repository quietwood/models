#!/bin/bash
# Copyright 2022 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================

if [ $# != 8 ]
then
    echo "Usage: bash run_infer_gpu.sh [SCENE_NAME] [POSE] [SCENE_CONFIG] [TRAIN_CONFIG] [DS_CONFIG] [HALLUCINATE_IMAGE_PATH] [CHECKPOINT_PATH] [OUT_PATH]"
exit 1
fi

get_real_path(){
  if [ "${1:0:1}" == "/" ]; then
    echo "$1"
  else
    echo "$(realpath -m $PWD/$1)"
  fi
}

SCENE_NAME=$1
POSE=$(get_real_path $2)
SCENE_CONFIG=$(get_real_path $3)
TRAIN_CONFIG=$(get_real_path $4)
DS_CONFIG=$(get_real_path $5)
HALLUCINATE_IMAGE_PATH=$(get_real_path $6)
CHECKPOINT_PATH=$(get_real_path $7)
OUT_PATH=$(get_real_path $8)

if [ ! -f $CHECKPOINT_PATH ]
then
    echo "error: CHECKPOINT_PATH=$CHECKPOINT_PATH is not a file"
exit 1
fi

echo "scene_name: "$SCENE_NAME
echo "pose: "$POSE
echo "scene_config: "$SCENE_CONFIG
echo "train_config: "$TRAIN_CONFIG
echo "ds_config: "$DS_CONFIG
echo "hallucinate_image_path: "$HALLUCINATE_IMAGE_PATH
echo "checkpoint_path: "$CHECKPOINT_PATH
echo "out_path: "$OUT_PATH

python infer.py --scene-name $SCENE_NAME --pose $POSE --scene-config $SCENE_CONFIG \
--data-config $DS_CONFIG --train-config $TRAIN_CONFIG --hallucinate-image-path $HALLUCINATE_IMAGE_PATH \
--model-ckpt $CHECKPOINT_PATH --out-path $OUT_PATH   &> infer_gpu.log &
