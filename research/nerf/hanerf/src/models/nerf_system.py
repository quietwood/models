# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
#
# This file or its part has been derived from the following repository
# and modified: https://github.com/rover-xingyu/Ha-NeRF
# ============================================================================
"""HA-NeRF predictions pipeline."""

import mindspore as ms
from mindspore import nn
from mindspore import ops

from .networks import ImplicitMask, EncodeAppearance
from ..volume_rendering.volume_rendering import VolumeRendering
from ..volume_rendering.scene_representation import PosEmbedding


class NeRFSystem(nn.Cell):
    def __init__(self, train_config, nerf_config):
        super().__init__()
        self.train_config = train_config

        self.embedding_xyz = PosEmbedding(nerf_config["n_emb_xyz"] - 1,
                                          nerf_config["n_emb_xyz"])
        self.embedding_dir = PosEmbedding(nerf_config["n_emb_dir"] - 1,
                                          nerf_config["n_emb_dir"])
        self.embedding_uv = PosEmbedding(10 - 1, 10)
        self.n_a = train_config["n_a"]
        self.n_vocab = train_config["n_vocab"]

        if train_config["encode_a"]:
            self.enc_a = EncodeAppearance(3, train_config["n_a"])
        self.step = ms.Parameter(ops.zeros((1,), dtype=ms.float32))
        # contain encode_appearance embeddings
        self.embedding_a_list = ms.Parameter(
            ops.zeros((train_config["n_vocab"],
                       train_config["n_a"]),
                      dtype=ms.float32))
        self.indexes_list = \
            ms.Parameter(-1 * ops.ones(train_config["n_vocab"],
                                       dtype=ms.float32))
        self.indexes_count = ms.Parameter(ops.zeros((1,), dtype=ms.float32))

        if train_config["use_mask"]:
            self.implicit_mask = ImplicitMask()
            self.embedding_view = nn.Embedding(train_config["n_vocab"], 128)

        self.network = VolumeRendering(
            embedding_xyz=self.embedding_xyz,
            embedding_dir=self.embedding_dir,
            nerf_config=nerf_config,
            train_config=train_config,
            hierarchical_samples=nerf_config["n_importance"],
            chunk=train_config["chunk"],
            perturbation=nerf_config["perturb"],
            n_samples=nerf_config["n_samples"],
            use_disp=nerf_config["use_disp"],
            noise_std=nerf_config["noise_std"]
        )

    def construct(self, rays, ts, whole_img, W, H, uv_sample):
        results = {}
        a_embedded_from_img = None
        a_embedded_random = None
        if self.train_config["encode_a"]:
            a_embedded_from_img = self.enc_a(whole_img)

            if self.train_config["encode_random"]:
                if self.step > 0:
                    indexes_count = self.indexes_count.astype(ms.int32)
                    if indexes_count < self.n_vocab:
                        output_min = min(ops.abs(self.indexes_list - ts[0]))
                        # if < 1e-5, this means that this index is already
                        # in the indexes_list, so we don't need to append it
                        if output_min > 1e-5:
                            self.indexes_list[indexes_count] = ts[0]
                            self.indexes_count += 1
                    # second argument is self.n_vocab because if we to use
                    # self.indexes_count instead of it an error occurs with it
                    list_ind = ops.randint(0, self.n_vocab, (1,),
                                           dtype=ms.int32)
                    list_ind = list_ind.astype(ms.int32)
                    # use % to limit our range from 0 to indexes_count,
                    # because before using self.n_vocab instead indexes_count
                    list_ind = list_ind % indexes_count
                    ind = self.indexes_list[list_ind]
                    ind = ind.astype(ms.int32)
                    a_embedded_random = self.embedding_a_list[ind].copy()
                else:
                    a_embedded_random = a_embedded_from_img.copy() + 1e-5

        # Do batched inference on rays using chunk
        rays_length = rays.shape[0]
        for i in range(0, rays_length, self.train_config["chunk"]):
            rendered_ray_chunks = \
                self.network(rays[i:i + self.train_config["chunk"]],
                             a_embedded_from_img,
                             a_embedded_random,
                             test_time=False)

            for key in rendered_ray_chunks:
                if key not in results:
                    results[key] = [rendered_ray_chunks[key]]
                else:
                    results[key] += [rendered_ray_chunks[key]]

        for key in results:
            results[key] = ops.cat(results[key], 0)

        if self.train_config["use_mask"]:
            uv_embedded = self.embedding_uv.embed(uv_sample)
            results['out_mask'] = \
                self.implicit_mask(ops.cat((self.embedding_view(ts),
                                            uv_embedded), -1))

        if self.train_config["encode_a"]:
            results['a_embedded'] = a_embedded_from_img
            if self.train_config["encode_random"]:
                results['a_embedded_random'] = a_embedded_random
                rec_img_random = results['rgb_fine_random'].view(1, H, W, 3)
                rec_img_random = rec_img_random.permute(0, 3, 1, 2) * 2 - 1
                results['a_embedded_random_rec'] = \
                    ops.stop_gradient(self.enc_a(rec_img_random))
                self.embedding_a_list[ts[0]] = \
                    ops.squeeze(a_embedded_from_img).copy()
                self.step = ops.stop_gradient(self.step)
                self.embedding_a_list = \
                    ops.stop_gradient(self.embedding_a_list)
                self.indexes_list = ops.stop_gradient(self.indexes_list)
                self.indexes_count = ops.stop_gradient(self.indexes_count)
        self.step += 1

        return results
