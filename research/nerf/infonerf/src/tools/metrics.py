# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
"""Metrics."""
import mindspore as ms
import numpy as np


class PSNR(ms.train.Metric):
    def __init__(self):
        super(PSNR, self).__init__()
        self.clear()

    def clear(self):
        """Initialize variables _abs_error_sum and _samples_num."""
        self._abs_error_sum = 0  # Save error sum.
        self._samples_num = 0  # Accumulated data volume.

    def update(self, *inputs):
        """Update _abs_error_sum and _samples_num."""
        y_pred = inputs[0]
        y_true = inputs[1].asnumpy()
        y_pred = y_pred[:, 3: 6].asnumpy()
        psnr = get_psnr(y_pred, y_true)

        if np.isinf(psnr):
            psnr = 60
        self._abs_error_sum += psnr
        self._samples_num += 1

    def eval(self):
        """Compute the final evaluation result."""
        return self._abs_error_sum / self._samples_num


def get_psnr(pr_img, gt_img):
    return -10. * np.log10(np.mean(np.square(pr_img - gt_img)))
