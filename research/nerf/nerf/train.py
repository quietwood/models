# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
"""NeRF train script."""

import argparse
import json
import os
from pathlib import Path
from typing import List

import mindspore as ms
import mindspore.nn as nn
from mindspore import context
from mindspore.communication.management import init, get_rank
from mindspore.context import ParallelMode
from mindspore.nn.loss.loss import LossBase
from mindspore.train.serialization import (
    load_checkpoint, load_param_into_net
)

import numpy as np

from src.data import create_datasets
from src.tools.common import get_callbacks
from src.volume_rendering import VolumeRendering


DEFAULT_MODEL_CONFIG = Path('src') / 'configs' / 'nerf_config.json'


def parse_args():
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    dataset = parser.add_argument_group('Dataset arguments.')
    dataset.add_argument('--data-path', type=Path, required=True,
                         help='Dataset directory.')
    dataset.add_argument('--data-config', type=Path, required=True,
                         help='Path to the dataset config.')

    train = parser.add_argument_group('Training arguments.')
    train.add_argument('--train-config', type=Path, required=True,
                       help='Path to the train config.')
    train.add_argument('--model-config', type=Path,
                       default=DEFAULT_MODEL_CONFIG,
                       help='Path to the model config.')
    train.add_argument('--init-ckpt', type=Path, default=None,
                       help='Path with pretrained model ckpt.')
    train.add_argument('--out-dir', type=Path, required=True,
                       help='Directory to save training model meta info.')
    train.add_argument('--mode', choices=['graph', 'pynative'],
                       default='graph',
                       help='Model representation mode. '
                            'Pynative for debugging.')

    runner = parser.add_argument_group('Runner context.')
    runner.add_argument('--num-workers', default=1, type=int,
                        help='Dataset number of parallel workers.')
    runner.add_argument('--device-type', default='GPU', type=str,
                        choices=['GPU', 'CPU'],
                        help='Training device.')
    runner.add_argument('--gpu-devices-num', type=int, default=1,
                        help='GPU devices number.')
    runner.add_argument('--gpu-devices-id', type=int, nargs='*',
                        default=[0],
                        help='GPU devices id.')

    args = parser.parse_args()
    if len(args.gpu_devices_id) != args.gpu_devices_num:
        args.gpu_devices_id = list(range(args.gpu_devices_num))
    print(args)
    return args


# Loss block.
class NerfL2Loss(LossBase):

    def __init__(self):
        super().__init__()
        self.reduce_mean = ms.ops.ReduceMean()
        self.pow = ms.ops.Pow()

    def construct(self, out, target):
        loss_coarse = self.reduce_mean(self.pow((out[:, :3] - target), 2))
        loss_coarse = ms.ops.clip_by_value(
            loss_coarse,
            clip_value_min=ms.Tensor(0.0),
            clip_value_max=ms.Tensor(1000.0)
        )
        loss_fine = self.reduce_mean(self.pow((out[:, 3: 6] - target), 2))
        loss_fine = ms.ops.clip_by_value(
            loss_fine,
            clip_value_min=ms.Tensor(0.0),
            clip_value_max=ms.Tensor(1000.0)
        )
        return loss_coarse + loss_fine


class PSNR(ms.train.Metric):
    def __init__(self):
        super(PSNR, self).__init__()
        self.reduce_mean = ms.ops.ReduceMean()
        self.pow = ms.ops.Pow()
        self.clear()

    def clear(self):
        """Initialize variables _abs_error_sum and _samples_num."""
        self._abs_error_sum = 0  # Save error sum.
        self._samples_num = 0    # Accumulated data volume.

    def update(self, *inputs):
        """Update _abs_error_sum and _samples_num."""
        y_pred = inputs[0]
        y_true = inputs[1]
        loss_fine = self.reduce_mean(self.pow((y_pred[:, 3: 6] - y_true), 2))
        psnr = -10.0 * (
            ms.numpy.log(loss_fine) / ms.numpy.log(ms.Tensor([10.0]))
        ).asnumpy()

        if np.isinf(psnr):
            psnr = 60
        self._abs_error_sum += psnr
        self._samples_num += 1

    def eval(self):
        """Compute the final evaluation result."""
        return self._abs_error_sum / self._samples_num


def load_ckpt(model, pretrained_weights, exclude_epoch_state=True):
    if os.path.isfile(pretrained_weights):
        print(f'=> loading pretrained weights from {pretrained_weights}')
        param_dict = load_checkpoint(pretrained_weights)
        last_epoch = 0
        if exclude_epoch_state:
            if 'epoch_num' in param_dict:
                last_epoch = int(param_dict['epoch_num'].asnumpy())
                param_dict.pop('epoch_num')
            if 'step_num' in param_dict:
                param_dict.pop('step_num')
            if 'learning_rate' in param_dict:
                param_dict.pop('learning_rate')
        load_param_into_net(model, param_dict)
    else:
        raise IOError(f'=> no pretrained weights found at '
                      f'{pretrained_weights}.')
    return last_epoch


def set_device(device_target: str,
               devices_num: int,
               devices_id: List[int]):
    """Set device and turn on distributed mode (if device_num > 1)"""
    if 'DEVICE_NUM' not in os.environ.keys():
        os.environ['DEVICE_NUM'] = str(devices_num)
        os.environ['RANK_SIZE'] = str(devices_num)

    env_device_num = int(os.environ.get('DEVICE_NUM', 1))
    rank = 0
    if device_target == 'GPU':
        if env_device_num > 1:
            init(backend_name='nccl')
            context.reset_auto_parallel_context()
            context.set_auto_parallel_context(
                device_num=env_device_num,
                parallel_mode=ParallelMode.DATA_PARALLEL,
                gradients_mean=True)
            rank = get_rank()
        else:
            context.set_context(device_target=device_target,
                                device_id=devices_id[rank])
    elif device_target == 'CPU':
        context.set_context(device_target=device_target)
    else:
        raise ValueError('Unsupported platform.')
    return rank


def set_context(mode: bool = False,
                device_target: str = 'GPU',
                devices_num: int = 1,
                devices_id=(0,)):
    ms_mode = ms.GRAPH_MODE if mode == 'graph' else ms.PYNATIVE_MODE
    context.set_context(
        mode=ms_mode,
        device_target=device_target,
        enable_graph_kernel=False
    )
    rank = set_device(device_target,
                      devices_num,
                      devices_id)
    return rank


def gen_lr(lr, decay_rate, train_epochs, ds_size):
    # As the reference repo.
    steps = train_epochs * ds_size
    decay_steps = steps * 0.25 + steps
    lr = [lr * decay_rate ** (step / decay_steps) for step in range(steps)]
    return lr


def calculate_recommended_hyperparams(rays_batch_size, ds_size):
    ref_rays_batch = 4096
    ref_iters = 200000
    ref_precrop_iters = 500

    one_epoch_rays = ds_size * rays_batch_size
    # number of epochs to train
    opt_epochs = (ref_rays_batch * ref_iters) // one_epoch_rays
    opt_precrop_epochs = (ref_rays_batch * ref_precrop_iters) // one_epoch_rays

    print(f'Reference repo rays batch size: {ref_rays_batch}')
    print(f'Reference repo iters: {ref_iters}')
    print(f'Reference repo precrop steps: {ref_precrop_iters}')
    print(f'Optimal epochs number for train rays batch size: {opt_epochs}')
    print(f'Optimal precrop epochs number for {rays_batch_size} train '
          f'rays batch size: {opt_precrop_epochs}')


def copy_configs_out(out_dir, scene_params, model_cfg, train_cfg, data_cfg):
    # Copy configs to the output directory.
    out_dir.mkdir(parents=True, exist_ok=True)
    scene_config_out = out_dir / 'scene_config.json'
    with open(scene_config_out, 'w') as f:
        json.dump(scene_params, f, indent=4)
    model_config_out = out_dir / 'model_config.json'
    with open(model_config_out, 'w') as f:
        json.dump(model_cfg, f, indent=4)
    train_config_out = out_dir / 'train_config.json'
    with open(train_config_out, 'w') as f:
        json.dump(train_cfg, f, indent=4)
    data_config_out = out_dir / 'data_config.json'
    with open(data_config_out, 'w') as f:
        json.dump(data_cfg, f, indent=4)


def main():
    args = parse_args()
    # Read configs.
    with open(args.train_config, 'r') as f:
        train_cfg = json.load(f)
    with open(args.data_config, 'r') as f:
        data_cfg = json.load(f)
    with open(args.model_config, 'r') as f:
        model_cfg = json.load(f)
    # Set context.
    rank = set_context(
        args.mode, args.device_type, args.gpu_devices_num, args.gpu_devices_id)
    # Dataset.
    train_ds, val_ds, scene_params = create_datasets(
        args.data_path,
        data_cfg,
        train_cfg['precrop_train_epochs'] > 0,
        train_cfg['precrop_train_frac'],
        train_cfg['train_rays_batch_size'],
        train_cfg['val_rays_batch_size'],
        train_shuffle=True,
        num_parallel_workers=args.num_workers,
        num_shards=args.gpu_devices_num,
        shard_id=rank)
    print(f'Scene parameters: {scene_params}')
    copy_configs_out(
        args.out_dir, scene_params, model_cfg, train_cfg, data_cfg
    )

    # Recommended parameters.
    calculate_recommended_hyperparams(train_cfg['train_rays_batch_size'],
                                      train_ds.get_dataset_size())
    # Create model and load checkpoints.
    print('Start the model initialization.')
    network = VolumeRendering(
        **model_cfg,
        linear_disparity_sampling=scene_params['linear_disparity_sampling'],
        perturbation=train_cfg['perturbation'],
        raw_noise_std=train_cfg['raw_noise_std'],
        white_bkgr=scene_params['white_background'],
        near=scene_params['near'],
        far=scene_params['far'])
    if args.init_ckpt is not None:
        ckpt_last_epoch = load_ckpt(network, str(args.init_ckpt))
    else:
        ckpt_last_epoch = 0
    # Set loss, evaluate model, init optimizer.
    loss = NerfL2Loss()
    eval_network = nn.WithEvalCell(network, loss)
    lr = gen_lr(
        train_cfg['lr'], train_cfg['lr_decay_rate'], train_cfg['epochs'],
        train_ds.get_dataset_size())
    opt = nn.AdamWeightDecay(
        params=network.trainable_params(), learning_rate=ms.Tensor(lr))
    model = ms.Model(
        network, loss_fn=loss, optimizer=opt, metrics={'PSNR': PSNR()},
        eval_network=eval_network, eval_indexes=[0, 1, 2]
    )
    # Callbacks.
    cb = get_callbacks(
        'nerf', rank, train_ds.get_dataset_size(),
        val_ds.get_dataset_size(), args.out_dir,
        ckpt_save_every_step=0, ckpt_save_every_sec=0, ckpt_keep_num=5,
        print_loss_every=100, collect_graph=True)

    # Train.
    assert train_cfg['precrop_train_epochs'] > 0
    epochs = train_cfg['epochs'] if train_cfg['precrop_train_epochs'] == 0 \
        else train_cfg['precrop_train_epochs']
    if ckpt_last_epoch < epochs:
        print(f'Start training from {ckpt_last_epoch}.')
        model.fit(epochs,
                  train_ds,
                  callbacks=cb,
                  valid_dataset=val_ds,
                  valid_frequency=train_cfg['val_epochs_freq'],
                  initial_epoch=ckpt_last_epoch)
    if train_cfg['precrop_train_epochs'] > 0:
        print('Update ds and continue training.')
        train_ds, val_ds, _ = create_datasets(
            args.data_path,
            data_cfg,
            False,
            train_cfg['precrop_train_frac'],
            train_cfg['train_rays_batch_size'],
            train_cfg['val_rays_batch_size'],
            train_shuffle=True,
            num_parallel_workers=args.num_workers,
            num_shards=args.gpu_devices_num,
            shard_id=rank)
        init_epoch = ckpt_last_epoch \
            if ckpt_last_epoch > train_cfg['precrop_train_epochs'] \
            else train_cfg['precrop_train_epochs']
        print(f'Continue training from {init_epoch}.')
        model.fit(train_cfg['epochs'],
                  train_ds,
                  callbacks=cb,
                  valid_dataset=val_ds,
                  valid_frequency=train_cfg['val_epochs_freq'],
                  initial_epoch=init_epoch)


if __name__ == '__main__':
    main()
