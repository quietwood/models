# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================


import numpy as np
import mindspore as ms
from mindspore import nn, ops, Tensor


class Featureslinear(nn.Cell):
    def __init__(self, field_dims, output_dim=1):
        super(Featureslinear, self).__init__()
        self.fc = nn.Embedding(sum(field_dims), output_dim)
        self.bias = ms.Parameter(ops.zeros((output_dim,)))
        self.offsets = Tensor([0, *np.cumsum([field_dims])[:-1]])

    def construct(self, x):
        x = x + self.offsets
        return ops.sum(self.fc(x), dim=1) + self.bias


class Featuresembedding(nn.Cell):
    def __init__(self, field_dims, embed_dim):
        super(Featuresembedding, self).__init__()
        self.embedding = nn.Embedding(sum(field_dims), embed_dim)
        self.offsets = Tensor([0, *np.cumsum(field_dims)[:-1]])

    def construct(self, x):
        x = x + self.offsets
        return self.embedding(x)


class Mlp(nn.Cell):
    def __init__(self, input_dim, embed_dims, dropout, output_layer=True):
        super(Mlp, self).__init__()
        layers = list()
        for embed_dim in embed_dims:
            layers.append(nn.Dense(input_dim, embed_dim))
            layers.append(nn.BatchNorm1d(embed_dim))
            layers.append(nn.ReLU())
            layers.append(nn.Dropout(p=dropout))
            input_dim = embed_dim
        if output_layer:
            layers.append(nn.Dense(input_dim, 2))
        self.mlp = nn.SequentialCell(*layers)

    def construct(self, x):
        return self.mlp(x)


class Afmbase(nn.Cell):
    def __init__(self, embed_dim, attn_size, dropouts):
        super(Afmbase, self).__init__()
        self.attention = nn.Dense(embed_dim, attn_size)
        self.projection = nn.Dense(attn_size, 1)
        self.fc = nn.Dense(embed_dim, 1)
        self.dropouts = dropouts

    def construct(self, x):
        num_fields = x.shape[1]
        row, col = list(), list()
        for i in range(num_fields - 1):
            for j in range(i + 1, num_fields):
                row.append(i)
                col.append(j)
        p, q = x[:, row], x[:, col]
        inner_product = p * q
        attn_scores = ops.relu(self.attention(inner_product))
        attn_scores = ops.softmax(self.projection(attn_scores), axis=1)
        attn_scores = ops.dropout(attn_scores, p=self.dropouts[0])
        attn_output = ops.sum(attn_scores * inner_product, dim=1)
        attn_output = ops.dropout(attn_output, p=self.dropouts[1])
        return self.fc(attn_output)


def kmax_pooling(x, dim, k):
    index = x.topk(k, dim=dim)[1].sort(axis=dim)[0]
    return index
