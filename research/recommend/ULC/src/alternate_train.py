# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================

from copy import deepcopy

from models import get_model
from loss import get_loss_fn
from utils import get_optimizer
from metrics import cal_llloss_with_logits, cal_auc, cal_prauc
from data import get_criteo_dataset, RandomAccessDataset
from tqdm import tqdm
import numpy as np
import mindspore.dataset as ds
import mindspore.nn as nn
import mindspore

def test(model, test_data, params):
    all_logits = []
    all_probs = []
    all_labels = []
    model.set_train(False)

    for batch in tqdm(test_data):
        batch_x = batch[0]
        batch_y = batch[1]
        logits = model(batch_x)
        all_logits.append(logits.asnumpy())
        all_labels.append(batch_y.asnumpy())
        all_probs.append(nn.Sigmoid()(logits).asnumpy())

    all_logits = np.reshape(np.concatenate(all_logits, axis=0), (-1,))
    all_labels = np.reshape(np.concatenate(all_labels, axis=0), (-1,))
    all_probs = np.reshape(np.concatenate(all_probs, axis=0), (-1,))
    llloss = cal_llloss_with_logits(all_labels, all_logits)
    auc = cal_auc(all_labels, all_probs)
    prauc = cal_prauc(all_labels, all_probs)
    return auc, prauc, llloss

def get_valid_llloss_blc(model, test_data, correction_model):
    all_logits = []
    all_labels = []
    model.set_train(False)

    for batch in tqdm(test_data):
        batch_x = batch[0]
        batch_y = batch[1]
        logits0 = correction_model(batch_x)
        corrections = (nn.Sigmoid()(logits0)).flatten()
        corrections = mindspore.numpy.where(batch_y < 1, corrections, batch_y.float())
        logits = model(batch_x)
        all_logits.append(logits.asnumpy())
        all_labels.append(corrections.asnumpy())

    all_logits = np.reshape(np.concatenate(all_logits, axis=0), (-1,))
    all_labels = np.reshape(np.concatenate(all_labels, axis=0), (-1,))


    llloss = cal_llloss_with_logits(all_labels, all_logits)
    return llloss

def alternate_run(params, wandb):
    cvr_model = None
    sub_model = None
    dataset = get_criteo_dataset(params)
    sub_params = deepcopy(params)

    sub_params["dataset"] = "fsiwsg_cd_"+str(params["CD"])\
        +"_end_"+str(params["training_end_day"])+"_seed_"+str(params["seed"])
    np.random.seed(params["seed"])
    sub_dataset = get_criteo_dataset(sub_params)["train"]
    np.random.seed(params["seed"])

    params["log_step"] = 0
    params["idx"] = 1
    for _ in range(2):
        sub_model = sub_train(cvr_model, sub_dataset, params)
        cvr_model = cvr_train(sub_model, dataset, params, wandb)
        params["idx"] += 1

def sub_train(cvr_model, sub_dataset, params):
    train_data_x = sub_dataset["x"].to_numpy().astype(np.float32)
    train_data_label = sub_dataset["labels"]
    train_data_label = 1 - train_data_label
    train_data = RandomAccessDataset(train_data_x, train_data_label)
    train_data_loader = ds.GeneratorDataset(source=train_data, shuffle=True, column_names=['feature', 'label'])
    train_data_loader = train_data_loader.batch(batch_size=params["batch_size"])

    model = get_model("MLP_FSIW", params)

    if cvr_model is not None:
        sd = cvr_model.parameters_dict()
        part_sd = {k: v for k, v in sd.items() if ("category_embeddings" in k) or ("numeric_embeddings" in k)}
        model_dict = model.parameters_dict()
        model_dict.update(part_sd)
        mindspore.load_param_into_net(model, model_dict)

    optimizer = nn.Adam(params=model.trainable_params(), learning_rate=0.001, weight_decay=0)
    loss_fn = get_loss_fn("cross_entropy_loss")

    def forward_fn(data, label):
        outputs = model(data)
        targets = {"label": label}
        loss_dict = loss_fn(targets, outputs, params)
        loss = loss_dict["loss"]
        return loss

    grad_fn = mindspore.value_and_grad(forward_fn, None, optimizer.parameters)

    for _ in range(5):
        for batch in train_data_loader:
            batch_x = batch[0]
            batch_y = batch[1][:, 0]
            targets = {"label": batch_y}

            model.set_train(True)
            _, grads = grad_fn(batch_x, targets["label"])

            optimizer(grads)

    return model

def cvr_train(sub_model, datasets, params, wandb):
    model = get_model("MLP_SIG", params)
    models = {"model": model, "submodel": sub_model}

    optimizer = get_optimizer(models["model"].trainable_params(), params["optimizer"], params)

    train_dataset = datasets["train"]
    train_data_x = train_dataset["x"].to_numpy().astype(np.float32)
    train_data_label = train_dataset["labels"]
    train_data = RandomAccessDataset(train_data_x, train_data_label)
    train_data_loader = ds.GeneratorDataset(source=train_data, shuffle=True, column_names=['feature', 'label'])
    train_data_loader = train_data_loader.batch(batch_size=params["batch_size"])

    valid_dataset = datasets["valid"]
    valid_data_x = valid_dataset["x"].to_numpy().astype(np.float32)
    valid_data_label = valid_dataset["labels"]
    valid_data = RandomAccessDataset(valid_data_x, valid_data_label)
    valid_data_loader = ds.GeneratorDataset(source=valid_data, column_names=['feature', 'label'])
    valid_data_loader = valid_data_loader.batch(batch_size=params["batch_size"])

    test_dataset = datasets["test"]
    test_data_x = test_dataset["x"].to_numpy().astype(np.float32)
    test_data_label = test_dataset["labels"]
    test_data = RandomAccessDataset(test_data_x, test_data_label)
    test_data_loader = ds.GeneratorDataset(source=test_data, column_names=['feature', 'label'])
    test_data_loader = test_data_loader.batch(batch_size=params["batch_size"])

    data_loaders = {
        "train_data": train_data_loader,
        "test_data": test_data_loader,
        "valid_data": valid_data_loader
    }
    optimizers = {
        "optimizer": optimizer
    }


    return train(models, optimizers, data_loaders, params, wandb)


def train(models, optimizers, data_loaders, params, wandb):
    train_data = data_loaders["train_data"]
    valid_data = data_loaders["valid_data"]
    test_data = data_loaders["test_data"]
    best_model = None

    optimizer = optimizers["optimizer"]

    loss_fn = get_loss_fn(params["loss"])
    val_llloss = []
    test_auc, test_prauc, test_llloss = [], [], []

    def forward_fn(data, label):
        outputs = models["model"](data)
        logits0 = models["submodel"](data)
        correction_label = nn.Sigmoid()(logits0).flatten()
        label = mindspore.numpy.where(label < 1, correction_label, label.float())
        targets = {"label": label}
        loss_dict = loss_fn(targets, outputs, params)
        loss = loss_dict["loss"]

        return loss

    grad_fn = mindspore.value_and_grad(forward_fn, None, optimizer.parameters)

    for ep in range(params["train_epoch"]):
        vllloss = get_valid_llloss_blc(models["model"], valid_data, models["submodel"])
        print("Val ep{}, llloss {}".format(ep, vllloss))
        tauc, tprauc, tllloss = test(models["model"], test_data, params)
        print("Test ep{}, auc {}, prauc {}, llloss {}".format(ep, tauc, tprauc, tllloss))

        if not val_llloss or vllloss < min(val_llloss):
            best_model = models["model"].parameters_dict()

        val_llloss.append(vllloss)
        test_auc.append(tauc)
        test_prauc.append(tprauc)
        test_llloss.append(tllloss)

        if len(val_llloss) - val_llloss.index(min(val_llloss)) > params["early_stop"]:
            best_ep = val_llloss.index(min(val_llloss))
            print("Early stop at ep {}. Best ep {}. Best val_lloss {}.".format(ep, best_ep, min(val_llloss)))
            print("Final test evaluation: auc {}, prauc {}, llloss {}."\
                  .format(test_auc[best_ep], test_prauc[best_ep], test_llloss[best_ep]))
            break
        train_loss = []
        for batch in tqdm(train_data):
            batch_x = batch[0]
            batch_y = batch[1]

            models["model"].set_train(True)
            models["submodel"].set_train(False)
            loss, grads = grad_fn(batch_x, batch_y)

            train_loss.append(loss.asnumpy())
            optimizer(grads)
            params["log_step"] += 1
        print("Train ep{}, loss {}".format(ep, np.mean(train_loss)))

    mindspore.load_param_into_net(models["model"], best_model)
    return models["model"]
