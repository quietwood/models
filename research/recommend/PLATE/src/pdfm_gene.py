# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================

import mindspore
import numpy as np

from layer import FactorizationMachine, FeaturesEmbedding, FeaturesLinear, MultiLayerPerceptron, MultiLayerPerceptron_normal


class PromptDeepFactorizationMachineModel_gene(mindspore.nn.Cell):
    """
    Reference:
        H Guo, et al. DeepFM: A Factorization-Machine based Neural Network for CTR Prediction, 2017.
    """

    def __init__(self, field_dims, embed_dim, mlp_dims, dropout, domain_id):
        super().__init__()
        pdim = np.array([4]) #the dimension of feature field for prompt should be 1
        fdim = np.array([4])
        for i in range(field_dims.shape[0]):
            fdim = np.append(fdim, [field_dims[i]])
        self.embed_dim = embed_dim
        self.domain_id = domain_id
        self.linear = FeaturesLinear(fdim)
        self.fm = FactorizationMachine(reduce_sum=True)
        self.embedding = FeaturesEmbedding(field_dims, embed_dim)
        self.embedding_prompt = FeaturesEmbedding(pdim, embed_dim)
        self.embed_output_dim = (len(field_dims)+2) * embed_dim
        self.mlp = MultiLayerPerceptron(self.embed_output_dim, mlp_dims, dropout)
        self.mlp_2 = MultiLayerPerceptron_normal(embed_dim, (32, 32), dropout)
    def Freeze1(self):
        for param in self.get_parameters():
            param.requires_grad = True
    def Freeze5(self):#tune prompt + linear
        for param in self.get_parameters():
            param.requires_grad = False
        for param in self.get_parameters():
            if "mlp_2.mlp" in param.name:
                param.requires_grad = True
            if 'linear' in param.name:
                param.requires_grad = True
        self.embedding_prompt.embedding.embedding_table.requires_grad = True
    def construct(self, x):
        """
        :param x: Long tensor of size ``(batch_size, num_fields)``
        """
        domain_id_l = x[:, 0]
        domain_id_l = domain_id_l.view(domain_id_l.shape[0], 1)
        lin = self.linear(x)
        x = x[:, 1:]
        embed_x = self.embedding(x)
        prompt = self.embedding_prompt(domain_id_l)
        user_prompt = self.mlp_2(embed_x[:, 0, :self.embed_dim])
        user_prompt = user_prompt.view(user_prompt.shape[0], 1, user_prompt.shape[1])
        embed_x = mindspore.ops.concat((prompt, user_prompt, embed_x), axis=1)
        x = lin + self.fm(embed_x) + self.mlp(embed_x.view(-1, self.embed_output_dim))
        return mindspore.ops.sigmoid(x.squeeze(1))
