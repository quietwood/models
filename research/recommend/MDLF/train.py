# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
"""
#################train MDLF example########################
python train.py
"""
from mindspore import context
from mindspore.communication.management import init
from mindspore.context import ParallelMode
from mindspore.train.model import Model
from mindspore.common import set_seed

from src.model_utils.moxing_adapter import moxing_wrapper
from src.model_utils.config import config
from src.model_utils.device_adapter import get_device_id, get_device_num, get_rank_id, get_job_id
from src.load_dataset import create_dataset
from src.model_builder import ModelBuilder
from src.MDLFMetric import MDLFMetric


def modelarts_pre_process():
    pass


set_seed(456)


@moxing_wrapper(pre_process=modelarts_pre_process)
def run_train():
    """train function"""
    print('device id:', get_device_id())
    print('device num:', get_device_num())
    print('rank id:', get_rank_id())
    print('job id:', get_job_id())

    device_target = config.device_target
    context.set_context(mode=context.GRAPH_MODE,
                        device_target=config.device_target)
    context.set_context(save_graphs=False)

    device_num = get_device_num()

    if config.run_distribute:
        context.reset_auto_parallel_context()
        context.set_auto_parallel_context(device_num=device_num,
                                          parallel_mode=ParallelMode.DATA_PARALLEL, gradients_mean=True)
        if device_target == "Ascend":
            context.set_context(device_id=get_device_id())
            init()
        elif device_target == "GPU":
            init()

    if config.run_distribute:
        context.reset_auto_parallel_context()
        context.set_auto_parallel_context(device_num=device_num,
                                          parallel_mode=ParallelMode.DATA_PARALLEL, gradients_mean=True)
        if device_target == "Ascend":
            context.set_context(device_id=get_device_id())
            init()
        elif device_target == "GPU":
            init()
    else:
        context.set_context(device_id=get_device_id())
    print("init finished.")

    ds_train = create_dataset(config.mindrecord_path, config.dataset, config.batch_size, training=True,
                              target=config.device_target, run_distribute=config.run_distribute)
    ds_eval = create_dataset(config.mindrecord_path, config.dataset, config.batch_size,
                             training=False, target=config.device_target)

    if ds_train.get_dataset_size() == 0:
        raise ValueError(
            "Please check dataset size > 0 and batch_size <= dataset size.")
    print("create dataset finished.")

    step_per_size = ds_train.get_dataset_size()
    print("train dataset size:", step_per_size)

    model_builder = ModelBuilder(config, step_per_size)
    train_net, eval_net = model_builder.get_train_eval_net()
    metric = MDLFMetric(config.intervals, config.length, config.interval_length)
    model = Model(train_net, eval_network=eval_net, metrics={'MyMetric': metric})
    callback_list = model_builder.get_callback_list(model, ds_eval)

    model.train(config.epochs, ds_train, callbacks=callback_list, dataset_sink_mode=False)


if __name__ == '__main__':
    run_train()
