# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================

import mindspore as ms
from mindspore import nn
import mindspore.ops as ops


class LFLossClass(nn.Cell):
    """
    NetWithLossClass definition.
    """

    def __init__(self, network, algorithm, arch_argv, loss_argv):
        super(LFLossClass, self).__init__(auto_prefix=False)
        self.network = network
        self.algorithm = algorithm
        self.K, self.N, self.interval_length = arch_argv
        self.N_ts = ms.Tensor(self.N, dtype=ms.int32)
        self.K_ts = ms.Tensor(self.K, dtype=ms.int32)
        self.zero_ts = ms.Tensor(0, dtype=ms.int32)
        self.one_ts = ms.Tensor(1, dtype=ms.int32)

        self.w_win_f, self.w_win_w, self.w_win_F, self.w_lose_F, self.neighbor = loss_argv

        if self.algorithm == 'MDLF':
            if self.w_lose_F != self.w_win_F:
                raise ValueError('w_lose_F should equal w_win_F')
        else:
            if self.w_win_w != 0:
                raise ValueError('Other than MDLF, w_win_w is 0.')
        self.range = ops.Range()
        self.masked_select = ops.MaskedSelect()
        self.mean = ops.ReduceMean()
        self.log = ops.Log()
        self.div = ops.DivNoNan()

    def construct(self, x, pos_lbl, score_lbl):
        """
        Construct LFLoss
        """
        batch = x.shape[0]
        size = self.N * self.K * batch
        pdf, cdf, winning_rate = self.network(x)

        cur_score = ops.clip_by_value((score_lbl / self.interval_length), self.zero_ts, self.N_ts - 1) \
            .reshape(-1, 1, 1).astype(ms.int32)
        cur_pos = ops.clip_by_value(pos_lbl, self.zero_ts, self.K_ts).reshape(-1, 1, 1).astype(ms.int32)

        mask_score = self.range(self.zero_ts, self.N_ts, self.one_ts).reshape(1, 1, self.N).broadcast_to(
            (batch, self.K, self.N)) == cur_score
        mask_win_pos = self.range(self.zero_ts, self.K_ts, self.one_ts).reshape(1, self.K, 1).broadcast_to(
            (batch, self.K, self.N)) >= cur_pos
        mask_log_pos = self.range(self.zero_ts, self.K_ts, self.one_ts).reshape(1, self.K, 1).broadcast_to(
            (batch, self.K, self.N)) == cur_pos
        mask_lose_pos = self.range(self.zero_ts, self.K_ts, self.one_ts).reshape(1, self.K, 1).broadcast_to(
            (batch, self.K, self.N)) < cur_pos
        win_pos_score = ops.logical_and(mask_win_pos, mask_score)
        lose_pos_score = ops.logical_and(mask_lose_pos, mask_score)
        log_pos_score = ops.logical_and(mask_log_pos, mask_score)

        if self.algorithm == 'ADM':
            win_neigh_cdf = ops.concat(
                [cdf[:, :, :self.neighbor], cdf[:, :, self.neighbor:] - cdf[:, :, :-self.neighbor]], axis=2)
            lose_neigh_cdf = ops.concat(
                [cdf[:, :, self.neighbor:] - cdf[:, :, :-self.neighbor], cdf[:, :, -self.neighbor:]], axis=2)
            loss_lose_F = -self.div(
                self.log((1 - lose_neigh_cdf) * lose_pos_score + 1e-5).sum() - (size - lose_pos_score.sum()) * self.log(
                    ms.Tensor(1e-5)), lose_pos_score.sum())
            loss_win_F = -self.div(
                self.log(win_neigh_cdf * win_pos_score + 1e-20).sum() - (size - win_pos_score.sum()) * self.log(
                    ms.Tensor(1e-20)), win_pos_score.sum())
        else:
            loss_lose_F = -self.div(
                self.log((1 - cdf) * lose_pos_score + 1e-5).sum() - (size - lose_pos_score.sum()) * self.log(
                    ms.Tensor(1e-5)), lose_pos_score.sum())
            loss_win_F = -self.div(
                self.log(cdf * win_pos_score + 1e-20).sum() - (size - win_pos_score.sum()) * self.log(ms.Tensor(1e-20)),
                win_pos_score.sum())

        loss_win_f = -self.div(
            self.log(pdf * log_pos_score + 1e-20).sum() - (size - log_pos_score.sum()) * self.log(ms.Tensor(1e-20)),
            log_pos_score.sum())
        loss_win_w = -self.div(
            self.log(winning_rate * log_pos_score + 1e-20).sum() - (size - log_pos_score.sum()) * self.log(
                ms.Tensor(1e-20)), log_pos_score.sum())

        if self.algorithm == 'MDLF':
            tot_loss = self.w_win_f * loss_win_f + self.w_win_w * loss_win_w + \
                       self.w_win_F * loss_win_F + self.w_lose_F * loss_lose_F
            return tot_loss, ops.stack((loss_win_f, loss_win_w, loss_lose_F, loss_win_F, tot_loss))

        tot_loss = self.w_win_f * loss_win_f + self.w_win_F * loss_win_F + self.w_lose_F * loss_lose_F
        return tot_loss, ops.stack((loss_win_f, ms.Tensor(0.), loss_lose_F, loss_win_F, tot_loss))


class SALossClass(nn.Cell):
    def __init__(self, network, algorithm, arch_argv, loss_argv, temperature=0.7):
        super(SALossClass, self).__init__(auto_prefix=False)
        self.network = network
        self.algorithm = algorithm
        self.K, self.N, self.interval_length = arch_argv
        self.w_l1, self.w_l2, self.w_l3, self.w_l4 = loss_argv
        self.temperature = temperature

        if algorithm == 'DeepHit':
            if self.w_l4 != 0:
                raise ValueError('For DeepHit, w_l4 is 0.')
        self.N_ts = ms.Tensor(self.N, dtype=ms.int32)
        self.K_ts = ms.Tensor(self.K, dtype=ms.int32)
        self.zero_ts = ms.Tensor(0, dtype=ms.int32)
        self.one_ts = ms.Tensor(1, dtype=ms.int32)
        self.range = ops.Range()
        self.mean = ops.ReduceMean()
        self.log = ops.Log()
        self.div = ops.DivNoNan()
        self.transpose = ops.Transpose()

    def construct(self, x, pos_lbl, score_lbl):
        """
        Construct SALoss
        """
        batch = x.shape[0]
        size = batch * self.K * self.N
        pdf, cdf, _ = self.network(x)

        cur_score = ops.clip_by_value((score_lbl / self.interval_length).astype(ms.int32), self.zero_ts,
                                      self.N_ts - 1).reshape(-1, 1, 1)
        cur_pos = ops.clip_by_value(pos_lbl, self.zero_ts, self.K_ts).reshape(-1, 1, 1)

        mask_score = (self.range(self.zero_ts, self.N_ts, self.one_ts).reshape(1, 1, -1)
                      .broadcast_to((batch, self.K, self.N)) == cur_score)
        mask_win_pos = (self.range(self.zero_ts, self.K_ts, self.one_ts).reshape(1, -1, 1)
                        .broadcast_to((batch, self.K, self.N)) >= cur_pos)
        mask_log_pos = (self.range(self.zero_ts, self.K_ts, self.one_ts).reshape(1, -1, 1)
                        .broadcast_to((batch, self.K, self.N)) == cur_pos)
        mask_lose_pos = ~mask_win_pos
        lose_pos_score = ops.logical_and(mask_lose_pos, mask_score)
        log_pos_score = ops.logical_and(mask_log_pos, mask_score)

        loss1 = -self.div(
            self.log(pdf * log_pos_score + 1e-20).sum() - (size - log_pos_score.sum()) * self.log(ms.Tensor(1e-20)),
            log_pos_score.sum())
        loss2 = -self.div(
            self.log((1 - cdf) * lose_pos_score + 1e-5).sum() - (size - lose_pos_score.sum()) * self.log(
                ms.Tensor(1e-5)), lose_pos_score.sum())

        # rank loss
        cur_score = cur_score.reshape(-1, 1)
        cur_pos = cur_pos.reshape(-1, 1)
        less_score_mat = cur_score.view(-1, 1) < cur_score.view(1, -1)  # _ij denotes s_i < s_j
        rank_loss_1 = ms.Tensor(0.)
        rank_loss_2 = ms.Tensor(0.)
        cdf_rev = self.transpose(cdf, (2, 1, 0))
        for k in range(self.K):
            F_k = cdf[:, k, :]
            F_k_t = cdf_rev[:, k, :]

            pos_k_label = cur_pos <= k
            pos_k_F = ops.logical_and((pos_k_label.broadcast_to((batch, batch))),
                                      less_score_mat)  # _ij means sample i ranks top-k and s_i < s_j
            self_F = ops.GatherD()(F_k, 1, cur_score).broadcast_to((batch, batch))
            pair_F = ops.Gather()(F_k_t, cur_score.squeeze(), 0)
            candidate = ops.exp(-(self_F - pair_F) / self.temperature)

            oj_score = ops.Select()(ops.logical_and(pos_k_label, (cur_score > 0)), cur_score - 1, cur_score)
            pair_F_2 = ops.GatherD()(F_k, 1, oj_score).broadcast_to((batch, batch))
            self_F_2 = ops.Gather()(F_k_t, oj_score.squeeze(), 0)
            candidate_2 = ops.exp(-(self_F_2 - pair_F_2) / self.temperature)

            rank_loss_1 += self.div((candidate * pos_k_F).sum(), pos_k_F.sum()) / self.K
            rank_loss_2 += self.div((candidate_2 * pos_k_F).sum(), pos_k_F.sum()) / self.K

        loss = loss1 * self.w_l1 + loss2 * self.w_l2 + rank_loss_1 * self.w_l3 + rank_loss_2 * self.w_l4
        return loss, ops.stack((loss1, loss2, rank_loss_1, rank_loss_2, loss))
