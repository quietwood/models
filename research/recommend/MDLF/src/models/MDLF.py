# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================

import mindspore as ms
import mindspore.nn as nn
import mindspore.ops as ops


class LandscapeModelRNN(nn.Cell):
    def __init__(self, feature_num, embedding_size_step=0, embedding_size=8, state_size=20,
                 intervals=100, length=5, MAX_SPARSE=110):
        super(LandscapeModelRNN, self).__init__()
        self.embedding_size = embedding_size
        self.embedding_size_step = embedding_size_step
        self.feature_num = feature_num
        self.K = length
        self.N = intervals
        self.state_size = state_size

        self.cells_num = self.N
        self.op_num = self.K
        if self.embedding_size > 0:
            self.embeddings = nn.Embedding(MAX_SPARSE, self.embedding_size)
        if self.embedding_size_step > 0:
            self.embeddings_steps = nn.Embedding(self.cells_num, self.embedding_size_step)

        self.init_h = ops.Zeros()((1, 1, self.state_size), ms.float32)
        self.init_c = ops.Zeros()((1, 1, self.state_size), ms.float32)
        self.Sigmoid = ops.Sigmoid()
        self.Concat_1 = ops.Concat(axis=1)
        self.Concat_2 = ops.Concat(axis=2)
        self.CumProd = ops.CumProd()
        self.Transpose = ops.Transpose()
        self.Range = nn.Range(0, self.cells_num, 1)

    def get_init_input(self, x):
        steps = self.Range()
        if self.embedding_size_step > 0:
            ts = self.embeddings_steps(steps).reshape(1, -1, self.embedding_size_step).repeat(len(x), 0)
        else:
            ts = steps.reshape(1, -1, 1).astype(ms.float32).repeat(len(x), 0)
        if self.embedding_size > 0:
            embeds = self.embeddings(x).reshape(-1, 1, self.feature_num * self.embedding_size).repeat(self.cells_num, 1)
            inputs = self.Concat_2([ts, embeds])
        else:
            inputs = ts  # only timestep
        return inputs


class MDLF(LandscapeModelRNN):
    def __init__(self, feature_num, embedding_size_step=0, embedding_size=8, state_size=20,
                 intervals=100, length=5, MAX_SPARSE=110):
        super(MDLF, self).__init__(feature_num, embedding_size_step, embedding_size, state_size,
                                   intervals, length, MAX_SPARSE)
        lstm_input_size = self.feature_num * self.embedding_size + max(self.embedding_size_step, 1)
        self.cell = nn.LSTM(input_size=lstm_input_size, hidden_size=self.state_size, batch_first=True)
        self.linear_layer = nn.Dense(in_channels=self.state_size, out_channels=self.op_num)
        print('Initialization Done')

    def construct(self, x):
        inputs = self.get_init_input(x)
        init_h = self.init_h.repeat(len(x), 1)
        init_c = self.init_c.repeat(len(x), 1)
        lstm_output, _ = self.cell(inputs, (init_h, init_c))
        linear_output = self.linear_layer(lstm_output)  # [batch, cells_num, op_num]
        output = self.Sigmoid(self.Transpose(linear_output, (0, 2, 1)))
        not_cdf = self.CumProd(self.CumProd(output, 1), 2)
        cdf = 1 - not_cdf
        pdf = self.Concat_2((1 - not_cdf[:, :, 0:1], not_cdf[:, :, :-1] - not_cdf[:, :, 1:]))
        winning_rate = self.Concat_1((1 - not_cdf[:, 0:1, :], not_cdf[:, :-1, :] - not_cdf[:, 1:, :]))
        return pdf, cdf, winning_rate


class BasicDLF(LandscapeModelRNN):
    def __init__(self, feature_num, embedding_size_step=0, embedding_size=8, state_size=20,
                 intervals=100, length=5, MAX_SPARSE=110):
        super(BasicDLF, self).__init__(feature_num, embedding_size_step, embedding_size, state_size,
                                       intervals, length, MAX_SPARSE)
        self.cells_list = nn.CellList()
        self.linears_list = nn.CellList()
        lstm_input_size = self.feature_num * self.embedding_size + max(self.embedding_size_step, 1)
        for _ in range(self.K):
            self.cells_list.append(nn.LSTM(input_size=lstm_input_size, hidden_size=self.state_size,
                            has_bias=True, batch_first=True))
            self.linears_list.append(nn.Dense(in_channels=self.state_size, out_channels=1))

    def construct(self, x):
        inputs = self.get_init_input(x)
        init_h = self.init_h.repeat(len(x), 1)
        init_c = self.init_c.repeat(len(x), 1)
        linear_outputs = []
        for i in range(self.K):
            output, _ = self.cells_list[i](inputs, (init_h, init_c))
            output = self.linears_list[i](output.reshape(-1, self.state_size))
            linear_outputs.append(output.reshape(-1, self.N))
        outputs = self.Sigmoid(ops.Stack(axis=1)(linear_outputs))
        not_h = 1 - outputs
        not_cdf = self.CumProd(not_h, 2)
        cdf = 1 - not_cdf
        pdf = self.Concat_2((1 - not_cdf[:, :, 0:1], not_cdf[:, :, :-1] - not_cdf[:, :, 1:]))
        winning_rate = self.Concat_1((1 - not_cdf[:, 0:1, :], not_cdf[:, :-1, :] - not_cdf[:, 1:, :]))
        return pdf, cdf, winning_rate
