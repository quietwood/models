# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
import os
from sklearn.cluster import KMeans
import numpy as np
from tqdm import tqdm

SPEAKER_DIR = ""
if __name__ == "__main__":
    spk_embs = []
    spk_pseudo_label_dict = {}
    file_lists = []
    for root, dirs, files in os.walk(SPEAKER_DIR):
        for i, file in enumerate(tqdm(files)):
            spk_emb = np.load(os.path.join(root, file))
            spk_embs.append(spk_emb)
            file_lists.append(file.split(".")[0])

    print(len(spk_embs))
    N_CLUSTER = 100
    kmeans = KMeans(n_clusters=N_CLUSTER, verbose=1)
    spk_pseudo_label = kmeans.fit_predict(spk_embs)
    spk_cluster_center = kmeans.cluster_centers_
    np.save("", spk_pseudo_label)
    np.save("", spk_cluster_center)

for i, label in enumerate(tqdm(spk_pseudo_label)):
    spk_pseudo_label_dict[file_lists[i]] = label

np.save("", spk_pseudo_label_dict)
