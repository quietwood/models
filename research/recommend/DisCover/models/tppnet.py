# MIT License
#
# Copyright (c) 2020 zhesong yu
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
'''
Modified from: https://github.com/yzspku/CQTNet
'''
from collections import OrderedDict
from mindspore import ops, nn
from .basic_cell import BasicCell


def spp(x, pool_size):
    batch_num, _, height, _ = x.shape
    for i, sz in enumerate(pool_size):
        maxpool = nn.AdaptiveMaxPool2d((height, sz))
        if i == 0:
            out = maxpool(x).view(batch_num, -1)
        else:
            out = ops.cat((out, maxpool(x).view(batch_num, -1)), 1)
    return out


class CQTTPPNet(BasicCell):
    def __init__(self, latent_dim=300, class_num=10000):
        super().__init__()
        self.latent_dim = latent_dim
        self.class_num = class_num
        self.features = nn.SequentialCell(OrderedDict([
            ('conv0', nn.Conv2d(in_channels=1, out_channels=32, kernel_size=(36, 40),
                                stride=(1, 1), has_bias=False)),
            ('norm0', nn.BatchNorm2d(32)),
            ('relu0', nn.ReLU()),
            ('conv1', nn.Conv2d(in_channels=32, out_channels=64, kernel_size=(12, 3),
                                stride=(1, 2), has_bias=False)),
            ('norm1', nn.BatchNorm2d(64)),
            ('relu1', nn.ReLU()),
            ('conv2', nn.Conv2d(in_channels=64, out_channels=128, kernel_size=(3, 3),
                                stride=(1, 2), has_bias=False)),
            ('norm2', nn.BatchNorm2d(128)),
            ('relu2', nn.ReLU()),
            ('pool0', nn.AdaptiveMaxPool2d((1, None))),
        ]))
        self.conv = nn.SequentialCell(OrderedDict([
            ('conv0', nn.Conv2d(in_channels=128, out_channels=256, kernel_size=(1, 3), stride=(1, 2), has_bias=False)),
            ('norm0', nn.BatchNorm2d(256)),
            ('relu0', nn.ReLU()),

            ('conv1', nn.Conv2d(in_channels=256, out_channels=512, kernel_size=(1, 3), stride=(1, 2), has_bias=False)),
            ('norm1', nn.BatchNorm2d(512)),
            ('relu1', nn.ReLU()),

            ('conv2', nn.Conv2d(in_channels=512, out_channels=512, kernel_size=(1, 3), stride=(1, 2), has_bias=False)),
            ('norm2', nn.BatchNorm2d(512)),
            ('relu2', nn.ReLU()),
        ]))
        self.fc0 = nn.Dense(10 * 512, self.latent_dim)
        self.fc1 = nn.Dense(self.latent_dim, self.class_num)

    def construct(self, x):
        batch_size = x.shape[0]
        x = self.features(x)
        x = self.conv(x)
        x = spp(x, [4, 3, 2, 1])
        x = x.view(batch_size, -1)
        feature = self.fc0(x)
        return feature
