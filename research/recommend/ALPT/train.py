# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================

from mindspore import Model, context
from mindspore.train.callback import ModelCheckpoint, CheckpointConfig, TimeMonitor
from src.deep_and_cross import PredictWithSigmoid, TrainStepWrap, NetWithLossClass, DeepCrossModel, TrainStepWrap_ALPT
from src.callbacks import LossCallBack
from src.datasets import create_dataset, DataType
from src.config import DeepCrossConfig
from src.callbacks import EvalCallBack
from src.metrics import AUCMetric

def get_DCN_net(configure):
    DCN_net = DeepCrossModel(configure)
    loss_net = NetWithLossClass(DCN_net)
    if configure.emb_type == "alpt":
        train_net = TrainStepWrap_ALPT(loss_net)
    else:
        train_net = TrainStepWrap(loss_net)
    eval_net = PredictWithSigmoid(DCN_net)
    return train_net, eval_net

def test_train(configure):
    epochs = configure.epochs
    batch_size = configure.batch_size
    field_size = configure.field_size

    ds_train = create_dataset('./data/mindrecord', train_mode=True, epochs=1, batch_size=batch_size,
                              data_type=DataType.MINDRECORD, target_column=field_size + 1)
    ds_eval = create_dataset('./data/mindrecord', train_mode=False, epochs=1,
                             batch_size=batch_size, data_type=DataType.MINDRECORD, target_column=field_size+1)
    print("ds_train.size: {}".format(ds_train.get_dataset_size()))

    train_net, eval_net = get_DCN_net(configure)
    train_net.set_train()
    # model = Model(train_net)
    auc_metric = AUCMetric()
    model = Model(train_net, eval_network=eval_net, metrics={"auc": auc_metric})
    callback = LossCallBack(config=configure)
    ckptconfig = CheckpointConfig(save_checkpoint_steps=ds_train.get_dataset_size(), keep_checkpoint_max=5)
    ckpoint_cb = ModelCheckpoint(prefix='deepcross_train', directory=configure.ckpt_path, config=ckptconfig)
    model.train(epochs, ds_train, callbacks=[TimeMonitor(ds_train.get_dataset_size()),
                                              callback, ckpoint_cb], dataset_sink_mode=True)

    eval_callback = EvalCallBack(model, ds_eval, auc_metric, configure)
    model.eval(ds_eval, callbacks=eval_callback)

if __name__ == "__main__":
    config = DeepCrossConfig()
    config.argparse_init()
    # context.set_context(mode=context.PYNATIVE_MODE, device_target="CPU")
    context.set_context(mode=context.PYNATIVE_MODE, device_target='GPU', device_id=config.device_id)
    test_train(config)
