# Copyright 2021-2022 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================

import numpy as np
from mindspore import nn
from mindspore import Tensor, context
from mindspore import ParameterTuple
import mindspore.common.dtype as mstype
from mindspore.ops import functional as F
from mindspore.ops import composite as C
from mindspore.ops import operations as P
from mindspore.nn import Dropout
from mindspore.nn.optim import Adam
from mindspore.common.initializer import Uniform, initializer
from mindspore.ops import Squeeze
from mindspore.common.parameter import Parameter
import mindspore.ops as ops

from src.embedding import Embedding, Embedding_LPT, Embedding_ALPT
from datetime import datetime

def print_time(msg):
    current_time = datetime.now()
    current_time_str = current_time.strftime('%Y-%m-%d %H:%M:%S.%f')[:-3]
    print(msg, current_time_str)

ms_type = mstype.float32
ds_type = mstype.int32


def init_method(method, shape, name, max_val=1.0):
    if method in ['uniform']:
        params = Parameter(initializer(
            Uniform(max_val), shape, ms_type), name=name)
    elif method == "one":
        params = Parameter(initializer("ones", shape, ms_type), name=name)
    elif method == 'zero':
        params = Parameter(initializer("zeros", shape, ms_type), name=name)
    elif method == "normal":
        params = Parameter(initializer("normal", shape, ms_type), name=name)
    return params


class DenseLayer(nn.Cell):
    """
    Dense Layer for DCN Model;
    Containing: activation, matmul, bias_add;
    """

    def __init__(self, input_dim, output_dim, weight_bias_init=['normal', 'normal'], act_str='relu',
                 keep_prob=0.5, use_activation=True, convert_dtype=True, drop_out=False):
        super(DenseLayer, self).__init__()
        weight_init, bias_init = weight_bias_init
        self.weight = init_method(
            weight_init, [input_dim, output_dim], name="weight")
        self.bias = init_method(bias_init, [output_dim], name="bias")
        self.act_func = self._init_activation(act_str)
        self.matmul = P.MatMul(transpose_b=False)
        self.bias_add = P.BiasAdd()
        self.cast = P.Cast()
        self.dropout = Dropout(p=1 - keep_prob)
        self.use_activation = use_activation
        self.convert_dtype = convert_dtype
        self.drop_out = drop_out

    def _init_activation(self, act_str):
        act_str = act_str.lower()
        if act_str == "relu":
            act_func = P.ReLU()
        elif act_str == "sigmoid":
            act_func = P.Sigmoid()
        elif act_str == "tanh":
            act_func = P.Tanh()
        return act_func

    def construct(self, x):
        '''
        Construct Dense layer
        '''
        if self.training and self.drop_out:
            x = self.dropout(x)
        if self.convert_dtype:
            x = self.cast(x, mstype.float16)
            weight = self.cast(self.weight, mstype.float16)
            bias = self.cast(self.bias, mstype.float16)
            wx = self.matmul(x, weight)
            wx = self.bias_add(wx, bias)
            if self.use_activation:
                wx = self.act_func(wx)
            wx = self.cast(wx, mstype.float32)
        else:
            wx = self.matmul(x, self.weight)
            wx = self.bias_add(wx, self.bias)
            if self.use_activation:
                wx = self.act_func(wx)
        return wx


class CrossLayer(nn.Cell):
    """
    Cross Layer for  DCN Model;
    """
    def __init__(self, cross_raw_dim, cross_col_dim, weight_bias_init, convert_dtype=True):
        super(CrossLayer, self).__init__()
        weight_init, bias_init = weight_bias_init
        self.cross_weight = init_method(weight_init, [cross_col_dim, 1], name="weight")
        self.cross_bias = init_method(bias_init, [cross_col_dim, 1], name="bias")
        self.matmul = nn.MatMul()
        self.bias_add = P.BiasAdd()
        self.tensor_add = P.Add()
        self.reshape = P.Reshape()
        self.cast = P.Cast()
        self.expand_dims = P.ExpandDims()
        self.convert_dtype = convert_dtype
        self.squeeze = Squeeze(2)

    def construct(self, inputs, x_0):
        '''
        Construct Cross layer
        '''
        x_0 = self.expand_dims(x_0, 2)
        x_l = self.expand_dims(inputs, 2)
        x_lw = C.tensor_dot(x_l, self.cross_weight, ((1,), (0,)))
        dot = self.matmul(x_0, x_lw)
        y_l = dot + self.cross_bias + x_l
        y_l = self.squeeze(y_l)
        return y_l


class DeepCrossModel(nn.Cell):
    def __init__(self, config):
        super(DeepCrossModel, self).__init__()
        self.concat = P.Concat(axis=1)
        self.reshape = P.Reshape()
        self.deep_reshape = P.Reshape()
        self.deep_mul = P.Mul()
        self.vocab_size = config.vocab_size
        self.emb_dim = config.emb_dim
        self.batch_size = config.batch_size
        self.field_size = config.field_size
        self.input_size = self.field_size * self.emb_dim
        self.deep_layer_dim = config.deep_layer_dim
        self.emb_type = config.emb_type
        if self.emb_type == "fp32":
            self.embedding = Embedding(self.vocab_size, self.emb_dim)
        elif self.emb_type == "lpt":
            self.embedding = Embedding_LPT(self.vocab_size, self.emb_dim)
        elif self.emb_type == "alpt":
            self.embedding = Embedding_ALPT(self.vocab_size, self.emb_dim)
        self.cross_layer_num = config.cross_layer_num
        self.keep_prob = config.keep_prob

        self.cross_layer_1 = CrossLayer(self.batch_size, self.input_size, weight_bias_init=['normal', 'normal'], convert_dtype=False)
        self.cross_layer_2 = CrossLayer(self.batch_size, self.input_size, weight_bias_init=['normal', 'normal'], convert_dtype=False)
        self.cross_layer_3 = CrossLayer(self.batch_size, self.input_size, weight_bias_init=['normal', 'normal'], convert_dtype=False)
        self.cross_layer_4 = CrossLayer(self.batch_size, self.input_size, weight_bias_init=['normal', 'normal'], convert_dtype=False)
        self.cross_layer_5 = CrossLayer(self.batch_size, self.input_size, weight_bias_init=['normal', 'normal'], convert_dtype=False)
        self.cross_layer_6 = CrossLayer(self.batch_size, self.input_size, weight_bias_init=['normal', 'normal'], convert_dtype=False)
        self.dense_layer_1 = DenseLayer(self.input_size, self.deep_layer_dim[0], keep_prob=self.keep_prob, convert_dtype=False)
        self.dense_layer_2 = DenseLayer(self.deep_layer_dim[0], self.deep_layer_dim[1], keep_prob=self.keep_prob, convert_dtype=False)
        self.dense_layer_3 = DenseLayer(self.input_size + self.deep_layer_dim[1], 1, act_str="sigmoid",
                                        keep_prob=self.keep_prob, use_activation=False, convert_dtype=False)

    def construct(self, id_hldr, wt_hldr):
        mask = self.reshape(wt_hldr, (self.batch_size, self.field_size, 1))
        deep_id_embs, _ = self.embedding(id_hldr)
        vx = self.deep_mul(deep_id_embs, mask)
        input_x = self.deep_reshape(vx, (-1, self.field_size * self.emb_dim))
        d_1 = self.dense_layer_1(input_x)
        d_2 = self.dense_layer_2(d_1)
        c_1 = self.cross_layer_1(input_x, input_x)
        c_2 = self.cross_layer_2(c_1, input_x)
        c_3 = self.cross_layer_3(c_2, input_x)
        c_4 = self.cross_layer_4(c_3, input_x)
        c_5 = self.cross_layer_5(c_4, input_x)
        c_6 = self.cross_layer_6(c_5, input_x)
        out = self.concat((d_2, c_6))
        out = self.dense_layer_3(out)
        return out


class NetWithLossClass(nn.Cell):
    """
    NetWithLossClass definition.
    """
    def __init__(self, network):
        super(NetWithLossClass, self).__init__(auto_prefix=False)
        self.loss = P.SigmoidCrossEntropyWithLogits()
        self.network = network
        self.ReduceMean = P.ReduceMean(keep_dims=False)

    def construct(self, batch_ids, batch_wts, label):
        predict = self.network(batch_ids, batch_wts)
        log_loss = self.loss(predict, label)
        mean_log_loss = self.ReduceMean(log_loss)
        loss = mean_log_loss
        return loss


class TrainStepWrap(nn.Cell):
    """
    TrainStepWrap definition
    """
    def __init__(self, network, lr=0.001, eps=1e-8, loss_scale=1.0):
        super(TrainStepWrap, self).__init__(auto_prefix=False)
        self.network = network
        self.network.set_grad()
        self.network.set_train()        
        self.weights = ParameterTuple(network.trainable_params())
        self.optimizer = Adam(self.weights, learning_rate=lr, eps=eps, loss_scale=loss_scale)
        self.hyper_map = C.HyperMap()
        self.grad = C.GradOperation(get_by_list=True, sens_param=True)
        self.sens = loss_scale       

    def construct(self, batch_ids, batch_wts, label):
        loss = self.network(batch_ids, batch_wts, label)
        sens = P.Fill()(P.DType()(loss), P.Shape()(loss), self.sens)
        grads = self.grad(self.network, self.weights)(batch_ids, batch_wts, label, sens)
        output = F.depend(loss, self.optimizer(grads))


        if hasattr(self.network.network.embedding, 'post_quantization'):
            self.network.network.embedding.post_quantization()

        return output


class TrainStepWrap_ALPT(nn.Cell):
    def __init__(self, network, lr=0.001, lr_alpha=5e-6, eps=1e-8, loss_scale=1.0):
        super(TrainStepWrap_ALPT, self).__init__(auto_prefix=False)
        self.network = network
        self.network.set_grad()
        self.network.set_train()        
        params = network.trainable_params()
        del params[1]
        self.weights = ParameterTuple(params)
        self.alpha_weights = ParameterTuple([network.trainable_params()[1]])
        self.alpha_optimizer = Adam(self.alpha_weights, learning_rate=lr_alpha, eps=eps, loss_scale=loss_scale)
        self.alpha_optimizer.global_step.name = 'alpha_global_step'
        self.alpha_optimizer.learning_rate.name = 'alpha_learning_rate'
        self.alpha_optimizer.beta1_power.name = 'alpha_beta1_power'
        self.alpha_optimizer.beta2_power.name = 'alpha_beta2_power'
        self.optimizer = Adam(self.weights, learning_rate=lr, eps=eps, loss_scale=loss_scale)
        self.hyper_map = C.HyperMap()
        self.grad = C.GradOperation(get_by_list=True, sens_param=True)
        self.sens = loss_scale       

    def construct(self, batch_ids, batch_wts, label):        
        loss = self.network(batch_ids, batch_wts, label)
        sens = P.Fill()(P.DType()(loss), P.Shape()(loss), self.sens)
        grads = self.grad(self.network, self.weights)(batch_ids, batch_wts, label, sens)
        output = F.depend(loss, self.optimizer(grads))

        loss = self.network(batch_ids, batch_wts, label)
        sens = P.Fill()(P.DType()(loss), P.Shape()(loss), self.sens)
        grads = self.grad(self.network, self.alpha_weights)(batch_ids, batch_wts, label, sens)
        output = F.depend(loss, self.alpha_optimizer(grads))

        self.network.network.embedding.post_quantization()

        return output


class PredictWithSigmoid(nn.Cell):
    """
    Eval model with sigmoid.
    """
    def __init__(self, network):
        super(PredictWithSigmoid, self).__init__(auto_prefix=False)
        self.network = network
        self.sigmoid = P.Sigmoid()

    def construct(self, batch_ids, batch_wts, labels):
        logits = self.network(batch_ids, batch_wts)
        pred_probs = self.sigmoid(logits)
        return logits, pred_probs, labels
