# Note

This is an example code for the cross-domain model in CIKM 2023 "DFFM: Domain Facilitated Feature Modeling for CTR Prediction.". (Paper can be download from https://dl.acm.org/doi/10.1145/3583780.3615469)

![](./images/image.png)

# Dataset

- Ali-CCP (https://tianchi.aliyun.com/dataset/408)
- Ali-Mama (https://tianchi.aliyun.com/dataset/dataDetail?dataId=56)

# Environment Requirements(#contents)

- Hardware（GPU）
    - Prepare hardware environment with GPU processor.
- Framework
    - MindSpore-1.9.0 (https://www.mindspore.cn/install/en)
- Requirements
  - pandas
  - numpy
  - random
  - mindspre==1.9.0
  - tensorflow
  - h5py
- For more information, please check the resources below：
  - MindSpore Tutorials (https://www.mindspore.cn/tutorials/en/master/index.html)
  - MindSpore Python API (https://www.mindspore.cn/docs/en/master/index.html)

# Quick Start

## 脚本和样例代码

```DFFM
.
└─DFFM
  ├─README_CN.md
  ├─src
    ├─__init__.py                         # init文件
    ├─dffm.py                             # DFFM模型整体
    ├─loss.py                             # loss函数
    ├─utils.py                            # utils ，用于计算auc
  ├─train_dffm.py                         # 训练和测试文件
```

# Performance

Experimental Result only for reference:

- Dataset

![](./images/1701762371904_image.png)

- Result

![](./images/1701762329518_image.png)
