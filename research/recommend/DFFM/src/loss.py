# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the License);
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an AS IS BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
from mindspore import nn
from mindspore import ops as P


class Ctr_Loss(nn.Cell):
    '''
    negative log-likelihood loss function
    '''

    def __init__(self):
        super(Ctr_Loss, self).__init__()
        self.reducemean = P.ReduceMean()
        self.log = P.Log()

    def construct(self, y_hat, target_ph, aux_loss):
        loss = -self.reducemean(self.log(y_hat) * target_ph) + aux_loss
        return loss
